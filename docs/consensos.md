Consensos
=========

Lista de consensos a los que hemos llegado entre reuniones y mailing lists.

* Los commits se firman con llave pública.
	
	# la opción -S sirve para firmar con la llave pública por defecto.
	git commit -S -am "my signed commit"

* Las tascas se tratan como features. Las features se tratan separadamente en branches dedicados. Cada feature branch empieza por "feature/"

* Antes de hacer un merge a "develop" se presenta en la reunión.

* Nos reunimos semanalmente o el miércoles o el jueves. Preferiblemente en roig entre las 16h y las 19h.

* El branching model está explicado en docs/git.md

* Utilizamos pep8 salvo alguna excepción. Hay un hook pre-commit para git en la carpeta hooks/.

* Las actas de las reuniones deberán de estar en docs/actas/.

* Los tags de versión y el nombre de release se escriben en formato x.y.z .