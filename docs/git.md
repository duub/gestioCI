Organización del repositorio
============================

Git-Flow (el modelo)
--------------------

Git-Flow es un modelo de branching diseñado por Vincent Driessen.
Aquí la referencia: http://datasift.github.io/gitflow/IntroducingGitFlow.html

git-flow (el paquete)
---------------------

Para facilitar la utilización de dicho modelo existe un paquete en debian llamado "git-flow".
Es una capa de abstracción por encima de git.


Una vez instalado es suficiente ejecutar en la carpeta de un repo ya existente:

    git flow init

Las respuestas por defecto al prompt corresponden al setup de nuestro repo.


Crear un feature
----------------

Hemos decidido tratar las tasques como features. Por cada tasca abriremos un feature diferente, siempre desde la branch "develop".

En el caso de utilizar la extensión git-flow el caso es muy sencillo:

    git flow feature start [name]

    # Esto creará un branch llamado feature/[name] basado en el branch "develop" y hará automaticamente un checkout.

En el caso contrario:

    # Asegurate de estar en el branch "develop"
    git checkout develop

    # Crea una branch y pasa a trabajar en ella
    git checkout -b feature/[name]

Acabar con un feature
---------------------

Acabar con un feature implica haber hecho cambios en el branch correspondiente y haberlos asegurado con un commit. Damos por supuesto que la tasca está acabada y que queremos hacer un merge de los cambios a la branch "develop", borrar la branch del feature en cuestión, y volver a trabajar en "develop". Asumimos que te encuentras en el branch del feature en cuestión.

En el caso de utilizar la extensión git-flow el caso es muy sencillo:

    git flow feature finish [name]

En el caso contrario:

    # Pasar a la branch "develop"
    git checkout develop

    # Hacer un merge del feature
    git merge feature/[name]

    # Borrar el branch feature
    git branch -d feature/[name]


TODO
----

Explicar el proceso de release y como se llega a master.

