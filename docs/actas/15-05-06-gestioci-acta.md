Orden del día - Martes 5 de mayo de 2015
========================================


Informativos
------------

### Peripecias Pablo ###
* Las fotos se renderizaban como paths relativos y no absolutos
* Los estáticos estaban mal configurados
* Estaban cambiadas las urls
En fin: se complicó la subida a producción
* He subido tasques nuevas.

### Especificaciones tasca #966 ###
* Data d'alta = data_resolucio
* Nº Soci = compte_CES_assignat
* COOP = Nº Soci
* COOPERATIVA = cooperativa_assignada
* Correu = email
* Telefon = telefon
* Adreça = 1er de la lista
* Nom = 1er membre de referencia
* Cognom = 1er membre de referencia
* DNI = 1er membre de referencia
* Tipus de soci = individiual
Botón en dashboard

### docs/ en el redmine ###
Creado consensos.md
Update de git.md

### Sobre git-flow y como pasar a producción ###
Pequeño informativo de como funciona git-flow y como preparar master para producción
http://danielkummer.github.io/git-flow-cheatsheet/
https://gist.github.com/kristopherjohnson/8979538


Propuestas de merge
-------------------

### feature/vista-resumen ###
Feature correspondiente a la tasca #252
Se evaluará mañana por la tarde a las 15:30h!


Debates
-------

* Permisos de git a projectes.cooperativa.cat
calen més permisos per poder fer un merge d'una branca "feature" a la branca "develop"
[Ja s'han donat permisos als usuaris per poder-ho fer]

* mirror del repo vs projecte públic
[trabajaremos la propuesta mejor y la traeremos mas adelante]

