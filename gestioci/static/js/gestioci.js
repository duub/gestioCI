$(function () {
    // Init foundation
    $(document).foundation({
        tooltip: {
            hover_delay: 10
        }
    });

    $('select').selectize();

    // Masonry
    $('[data-masonry]').imagesLoaded(function () {
        $('[data-masonry]').masonry({
            itemSelector: '.group'
        });
    });

    // Datefields
    // Date
    $('[data-field-date] > input').attr('placeholder', 'Data').fdatepicker({
        format:    'yyyy-mm-dd',
        weekStart: 1
    });

    // Time
    $('[data-field-time] > input').attr('placeholder', 'Hora');

    // Datetime
    $('[data-field-datetime] > input:first-child').attr('placeholder', 'Data').fdatepicker({
        format:    'yyyy-mm-dd',
        weekStart: 1
    });

    $('[data-field-datetime] > input:last-child').attr('placeholder', 'Hora');

    // DNI Fields
    // Add "data-dni" attribute to the element for automatic check
    // Write an "*" on the letter for automagic guessing
    $('[data-dni]').keypress(function (e) {
        return completar_dni(e);
    }).keyup(function (e) {
        feedback_dni(e.target);
    }).change(function (e) {
        feedback_dni(e.target);
    }).change();
});
