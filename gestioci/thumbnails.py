from PIL import Image
import os
from django.conf import settings


def get_thumbnail_path_and_filename(image_path):
    pathname, filename = os.path.split(image_path)
    return os.path.join(pathname, settings.THUMBNAILS_SUBDIR), filename


def create_thumbnail(image_filepath):
    """Creates a thumbail unless when there is one already and it is up to date.
    Arguments are:
      * image_filepath -- the full path of the source image for the thumbnail
    Return value:
      * True -- the thumbnail was already there, and it was up to date, or it has been successfully created"
      * False -- something happened that prevented to check the existing thumbnail or to create a current one."""

    thumbnails_dir, thumbnail_filename = get_thumbnail_path_and_filename(image_filepath)

    # try:
    if not os.path.exists(thumbnails_dir):
        os.mkdir(thumbnails_dir)

    thumbnail_filepath = os.path.join(thumbnails_dir, thumbnail_filename)

    if os.path.exists(thumbnail_filepath):
        otime = os.path.getmtime(image_filepath)
        ttime = os.path.getmtime(thumbnail_filepath)
        if ttime >= otime:
            return True

    im = Image.open(image_filepath)
    im.thumbnail(settings.THUMBNAIL_MAX_SIZE, settings.THUMBNAIL_FILTER)
    im.save(thumbnail_filepath)
    return True
    # except:
    #     return False