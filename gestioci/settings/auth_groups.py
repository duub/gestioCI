# coding=utf-8

# En aquest fitxer no s'ha de traduïr res;
# és per això que ens permetem no respectar la convenció de que
# els literals no traduïbles es delimiten amb single quote; ho
# fem per què d'aquesta manera es poden escriure els apòstrofs
# sense escapar :)

RESPONSABLES_ALTA = "Responsables d'alta"
EXPORTADORES = "Exportadores"

# TODO figure a way to automatically fill this array with introspection
grups = [
    RESPONSABLES_ALTA,
    EXPORTADORES,
]
