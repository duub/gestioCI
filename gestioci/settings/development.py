"""
Settings specific to the DEVELOPMENT environment
"""

from .base import *

ENV = 'development'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
TEMPLATE_DEBUG = True
ALLOWED_HOSTS = []


# Feature Flags
FEATURE_FLAGS.update({
    'FLAG_ALTA_SOCIES': False,
    'FLAG_FAQ':         False,
    'FLAG_GECO':        False,
    'FLAG_LETTUCE':     False,
})

# installed apps
# INSTALLED_APPS += ('test_m2m_abstract',)


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases
# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.sqlite3',
#         'NAME': os.path.join(BASE_DIR, 'tmp/development.sqlite3'),
#     }
# }
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME':   'gestioci',
        'USER':   'user',
    }
}


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'gestioci/static/build/'),
)


MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'
