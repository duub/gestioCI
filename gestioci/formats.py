# coding=utf-8
from django.utils import timezone

dow_names = [u"dilluns", u"dimarts", u"dimecres", u"dijous", u"divendres", u"dissabte", u"diumenge"]
dow_shortnames = [u'dl', u"dm", u"dc", u"dj", u"dv", u"ds", u"dg"]
month_names = [u"gener", u"febrer", u"març", u"abril", u"maig", u"juny",
               u"juliol", u"agost", u"setembre", u"octubre", u"novembre", u"desembre"]

mesos_amb_article = [u"de gener", u"de febrer", u"de març", u"d'abril", u"de maig", u"de juny",
                     u"de juliol", u"d'agost", u"de setembre", u"d'octubre", u"de novembre", u"de desembre"]

month_shortnames = [u"gen", u"feb", u"mar", u"abr", u"mai", u"jun",
                    u"jul", u"ago", u"set", u"oct", u"nov", u"des"]


def format_data_i_hora(d, curt=False, amagar_any_actual=False):

    d = timezone.localtime(d)

    tt = d.timetuple()
    dow = dow_names[tt.tm_wday]
    dow_short = dow_shortnames[tt.tm_wday]
    day = tt.tm_mday
    month = mesos_amb_article[tt.tm_mon - 1]
    month_short = month_shortnames[tt.tm_mon - 1]
    year = tt.tm_year

    h = tt.tm_hour
    m = tt.tm_min

    if curt:
        result = u'%s %d %s' % (dow_short, day, month_short)
    else:
        result = u'%s %d %s' % (dow, day, month)

    if not amagar_any_actual:
        if curt:
            result += u" %d" % (year)
        else:
            result += u' de %d' % (year)

    result += u", %02d:%02d" % (h, m)

    return result


def format_data(d, curt=False, amagar_any_actual=False):
    tt = d.timetuple()
    dow = dow_names[tt.tm_wday]
    dow_short = dow_shortnames[tt.tm_wday]
    day = tt.tm_mday
    month = mesos_amb_article[tt.tm_mon - 1]
    month_short = month_shortnames[tt.tm_mon - 1]
    year = tt.tm_year

    if curt:
        result = u'%s %d %s' % (dow_short, day, month_short)
    else:
        result = u'%s %d %s' % (dow, day, month)

    if not amagar_any_actual:
        if curt:
            result += u" %d" % year
        else:
            result += u" de %d" % year

    return result