# coding=utf-8
from django.contrib.auth.models import User
from django.db import models


class Empresa(models.Model):

    nom = models.CharField(max_length=256)
    nom_fiscal = models.CharField(max_length=256)
    nif = models.CharField(verbose_name='NIF', max_length=64)
    telefon = models.CharField(max_length=64, null=True, blank=True)
    email = models.EmailField(blank=True, null=True)

    compte_corrent = models.CharField(max_length=34, null=True, blank=True,
                                      help_text=u"Codi IBAN (si no el sabeu, comenceu per ES77 "
                                                u"i afegiu el CCC a continuació")

    # TODO to be solved later; do we need an address here?
    # adreca_fiscal = models.ForeignKey('socies.Adreca', verbose_name="Adreça fiscal")

    usuaria_que_demana_revisio = models.ForeignKey(User, blank=True, null=True, related_name='+')
    # podem asumir que la data en que es demana la revisió es 'updated_at'
    pendent_de_revisio = models.BooleanField(default=True)
    vist_i_plau_usuaria = models.ForeignKey(User, blank=True, null=True, related_name='+')
    vist_i_plau_data = models.DateTimeField(blank=True, null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "empresa"
        verbose_name_plural = "empreses"

    def __unicode__(self):
        return u"%s - %s" % (self.nif, self.nom)



class Cooperativa(Empresa):

    class Meta:
        verbose_name = "cooperativa"
        verbose_name_plural = "cooperatives"


# tipus de relacions entre Cooperativa i Empresa
# s'enten que una Empresa pot tenir els dos rols de cara a la mateixa Cooperativa
TIPUS_RELACIO_PROVEIDOR = 'proveidor'
TIPUS_RELACIO_CLIENT = 'client'
TIPUS_RELACIONS = (
    (TIPUS_RELACIO_PROVEIDOR, u"Proveïdor"),
    (TIPUS_RELACIO_CLIENT, u"Client"),
)


class CodisComptables(models.Model):
    """
    s'enten "codis comptables de client i proveidor" des del punt de vista d'una cooperativa
    """

    cooperativa = models.ForeignKey(Cooperativa, related_name='+')
    empresa = models.ForeignKey(Empresa, related_name='+')
    relacio = models.CharField(max_length=16, choices=TIPUS_RELACIONS)

    codi_comptable = models.IntegerField(help_text=u"comença per 430 si és client de la cooperativa, "
                                                   u"per 400 si és proveïdor")


class EmpresaAsseguradora(Empresa):

    class Meta:
        verbose_name = u"empresa asseguradora"
        verbose_name_plural = u"empreses asseguradores"


class CompteCorrentCooperativa(models.Model):

    TIPUS_PER_COBRAR = 'per_cobrar'
    TIPUS_PER_PAGAR = 'per_pagar'
    TIPUS = (
        (TIPUS_PER_COBRAR, u"Per rebre ingresos"),
        (TIPUS_PER_PAGAR, u"Per fer o domiciliar pagaments"),
    )

    iban = models.CharField(max_length=34, null=True, blank=True,
                            help_text=u"Codi IBAN (si no el sabeu, comenceu per ES77 i afegiu el CCC a continuació")
    cooperativa = models.ForeignKey(Cooperativa)
    tipus = models.CharField(max_length=16, choices=TIPUS)
    comentaris = models.TextField(blank=True, null=True)


class IAE(models.Model):

    codi = models.CharField(max_length=6)
    nom = models.CharField(max_length=256)
