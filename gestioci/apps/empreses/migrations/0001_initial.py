# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Empresa',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nom', models.CharField(max_length=256)),
                ('nom_fiscal', models.CharField(max_length=256)),
                ('nif', models.CharField(max_length=64, verbose_name=b'NIF')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'empresa',
                'verbose_name_plural': 'empreses',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Cooperativa',
            fields=[
                ('empresa_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='empreses.Empresa')),
            ],
            options={
                'verbose_name': 'cooperativa',
                'verbose_name_plural': 'cooperatives',
            },
            bases=('empreses.empresa',),
        ),
        migrations.CreateModel(
            name='EmpresaAsseguradora',
            fields=[
                ('empresa_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='empreses.Empresa')),
            ],
            options={
                'verbose_name': 'empresa asseguradora',
                'verbose_name_plural': 'empreses asseguradores',
            },
            bases=('empreses.empresa',),
        ),
    ]
