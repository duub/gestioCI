from django.contrib import admin

from .models import AdrecaProjecteAutoocupat, Activitat, Persona, ContribucioCIC, Habilitat, GrupHabilitat, \
    HabilitatsContribucionsPersona


for model in (AdrecaProjecteAutoocupat,
              Activitat,
              Persona,
              HabilitatsContribucionsPersona,
              ContribucioCIC,
              Habilitat,
              GrupHabilitat):
    admin.site.register(model)
