# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from gestioci.apps.socies.helpers import normalitzacio_per_cerca, normalitzacio_per_cerca_email, \
    normalitzacio_per_cerca_telefon


def actualitzar_personacercable(apps, schema_editor):

    Persona = apps.get_model('socies', 'Persona')
    PersonaCercable = apps.get_model('socies', 'PersonaCercable')

    # des de les migracions no es poden cridar mètodes del model
    # com ara p.actualitza_camps_calculats(), p._nom_sencer() o
    # p._tots_els_detalls() — Hem de copiar aqui el codi que cal
    # per fer aquestes operacions:

    # còpia del mètode del mateix nom a socies.Persona
    def _actualitza_persona_cercable(persona):
        pc = persona._persona_cercable or PersonaCercable()
        pc.nom = normalitzacio_per_cerca(persona.nom)
        pc.cognom1 = normalitzacio_per_cerca(persona.cognom1)
        pc.cognom2 = normalitzacio_per_cerca(persona.cognom2)
        pc.pseudonim = normalitzacio_per_cerca(persona.pseudonim)
        pc.email = normalitzacio_per_cerca_email(persona.email)
        pc.telefon = normalitzacio_per_cerca_telefon(persona.telefon)
        pc.dni = normalitzacio_per_cerca(persona.dni)
        pc.compte_integralces = normalitzacio_per_cerca(persona.compte_integralces)

        pc.save()
        return pc

    for p in Persona.objects.all():
        # això no modifica els objectes Persona; no cal preservar updated_at ni res :)
        p._persona_cercable = _actualitza_persona_cercable(p)


class Migration(migrations.Migration):

    dependencies = [
        ('socies', '0003_persona_verbose_names'),
    ]

    operations = [
        migrations.AddField(
            model_name='personacercable',
            name='compte_integralces',
            field=models.CharField(max_length=16, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='personacercable',
            name='dni',
            field=models.CharField(max_length=64, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='persona',
            name='dni',
            field=models.CharField(max_length=64, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.RunPython(actualitzar_personacercable),
    ]
