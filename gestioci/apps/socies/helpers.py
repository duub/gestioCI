# coding=utf-8
import unicodedata
import re

def normalitzacio_per_cerca(text):
    if text:
        return unicodedata.normalize('NFKD', text).encode('ASCII', 'ignore').lower()
    return text


def normalitzacio_per_cerca_email(email):
    if email:
        return email.lower()
    return email


def normalitzacio_per_cerca_telefon(telefon):
    if telefon:
        return telefon.lower().\
            replace(' ', ''). \
            replace('+', '00'). \
            replace('(', ''). \
            replace(')', ''). \
            replace('-', '').\
            replace('.', '')
    return telefon


def es_dni_valid(text_dni):
    # https://es.wikibooks.org/wiki/Algoritmo_para_obtener_la_letra_del_NIF#Python
    if not isinstance(text_dni, basestring):
        return False
    r_dni = r'^(?P<numero>\d{8})(?P<lletra>[TRWAGMYFPDXBNJZSQVHLCKE])$'
    r = re.match(r_dni, text_dni.upper())
    if r:
        nif = 'TRWAGMYFPDXBNJZSQVHLCKE'
        num = int(r.group('numero'))
        return r.group('lletra') == nif[num % 23]
    return False


