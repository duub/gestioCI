# coding=utf-8
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseBadRequest
from cercadors import cerca_difusa_de_persones
import json


@login_required
def cerca_persones(request):

    if request.method == 'POST':

        parametres_de_la_cerca = {}
        for key in ['nom', 'cognom1', 'cognom2', 'pseudonim', 'email', 'telefon', 'dni']:
            value = request.POST.get(key, None)
            if value:
                value = value.strip()
                if len(value):
                    parametres_de_la_cerca[key] = value

        if parametres_de_la_cerca:

            coincidencies = cerca_difusa_de_persones(**parametres_de_la_cerca)

            result_array = []
            for coincidencia in coincidencies:
                persona_dict = {}
                persona = coincidencia['persona']
                persona_dict['id'] = persona.id
                persona_dict['punts'] = coincidencia['punts']
                persona_dict['text'] = persona.tots_els_detalls
                persona_dict['nom'] = persona.nom
                persona_dict['cognom1'] = persona.cognom1
                persona_dict['cognom2'] = persona.cognom2
                persona_dict['pseudonim'] = persona.pseudonim
                persona_dict['email'] = persona.email
                persona_dict['telefon'] = persona.telefon
                persona_dict['dni'] = persona.dni
                persona_dict['tipus_document_dni'] = persona.tipus_document_dni
                persona_dict['compte_integralces'] = persona.compte_integralces

                result_array.append(persona_dict)

            result = json.dumps(result_array)
        else:
            result = json.dumps([])

    else:
        return HttpResponseBadRequest(u"Què mireu? :)")

    return HttpResponse(result, content_type='application/json')
