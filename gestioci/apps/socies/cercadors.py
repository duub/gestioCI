# coding=utf-8
from helpers import normalitzacio_per_cerca, normalitzacio_per_cerca_email, normalitzacio_per_cerca_telefon
from models import PersonaCercable


def cerca_difusa_de_persones(limit=10,
                             nom=None,
                             cognom1=None,
                             cognom2=None,
                             pseudonim=None,
                             email=None,
                             telefon=None,
                             dni=None):
    """
    Retornem una llista de "dict" amb aquests elements per cada "dict", ordenats per puntuació descendent:
    * id: PK de l'objecte Persona
    * nom: __unicode__() de l'objecte persona
    * punts: puntuació segons l'algorisme que es descriu a continuació:

    Cerquem al model "Persona" extès amb el model "DadesFiscalsPersona".
    Assignem "pesos" a diferents coincidècies (una mica arbitrari; obert a millores):

    * 100 si coincideix el dni
    * 25 si coincideix el nom
    * 25 si coincideix el cognom1
    * 25 si coincideix el cognom2
    * 25 si coincideix el pseudònim
    * 85 si coincideix l'email
    * 85 si coincideix el telèfon

    Llavors multipliquem la puntuació per la quantitat de coincidències.

    Pensava sumar això però diria que no hi haurà gaire diferència, al final:
    * 175 si coincideix nom, cognom1, cognom2, dni
    * 100 si coincideix nom, cognom1, cognom2
    * 150 si coincideix nom, cognom1, dni
    * 100 si coincideix pseudònim, email
    * 185 si coincideix pseudònim, email, telèfon

    Possibles millores:
    * Avaluar possibilitat de "soundex" (no recordo el nom de l'algorisme modern) o equivalent.
    * Avaluar eficiència d'aquest mètode. Probablement no es podra cridar a cada OnChange() al form HTML.
    * Guardar versió "simple" de cada camp a Persona: tot minúscules, sense caracters fora de l'alfabet angles, ...
    """

    # TODO p.ex. "garcia de larrea" vs "garcia" hauria de donar alguna coincidència (amb poc pes?)
    # TODO probablement ens caldrà en algun moment limitar les cerques
    # TODO go postgres trigrams?? :)

    puntuacions_per_persona = {}
    coincidencies_per_persona = {}

    def actualitza(persones_queryset, punts, coincidencia):
        if persones_queryset.exists():
            for persona in persones_queryset:

                if persona.id in puntuacions_per_persona:
                    puntuacions_per_persona[persona.id] += punts
                else:
                    puntuacions_per_persona[persona.id] = punts

                if persona.id in coincidencies_per_persona:
                    coincidencies_per_persona[persona.id] += (coincidencia,)
                else:
                    coincidencies_per_persona[persona.id] = (coincidencia,)

    if dni:
        dni = normalitzacio_per_cerca(dni)
        actualitza(PersonaCercable.objects.filter(dni__contains=dni.lower()), 95, 'dni')
    if nom:
        nom = normalitzacio_per_cerca(nom)
        actualitza(PersonaCercable.objects.filter(nom__contains=nom.lower()), 25, 'nom')
    if cognom1:
        cognom1 = normalitzacio_per_cerca(cognom1)
        actualitza(PersonaCercable.objects.filter(cognom1__contains=cognom1.lower()), 25, 'cognom1')
    if cognom2:
        cognom2 = normalitzacio_per_cerca(cognom2)
        actualitza(PersonaCercable.objects.filter(cognom2__contains=cognom2.lower()), 25, 'cognom2')
    if pseudonim:
        pseudonim = normalitzacio_per_cerca(pseudonim)
        actualitza(PersonaCercable.objects.filter(pseudonim__contains=pseudonim.lower()), 25, 'pseudonim')
    if email:
        email = normalitzacio_per_cerca_email(email)
        actualitza(PersonaCercable.objects.filter(email__contains=email.lower()), 85, 'email')
    if telefon:
        telefon = normalitzacio_per_cerca_telefon(telefon)
        actualitza(PersonaCercable.objects.filter(telefon__contains=telefon.lower()), 85, 'telefon')

    for (pk, punts) in puntuacions_per_persona.iteritems():
        puntuacions_per_persona[pk] *= len(coincidencies_per_persona[pk])

    # donem la volta a (k: v) per poder ordenar per puntuació
    ranking = {}
    for k, v in puntuacions_per_persona.iteritems():
        ranking[v] = ranking.get(v, [])
        ranking[v].append(k)

    # preparem els resultats per retornar
    result = []
    for punts in sorted(ranking, reverse=True):
        for pk in ranking[punts]:
            result.append({'punts': punts, 'persona': PersonaCercable.objects.get(pk=pk).persona})
            if len(result) > limit:
                break

    return result