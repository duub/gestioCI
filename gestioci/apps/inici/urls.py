from django.conf.urls import patterns, url

urlpatterns = patterns(
    'gestioci.apps.inici.views',

    url(r'^/?$',           'index',      name='index'),
    url(r'^style-guide/$', 'styleguide', name='styleguide'),
)
