# coding=utf-8
from django.contrib import messages
from django.shortcuts import render


def index(request):
    return render(request, 'inici/index.html', {
        'page_title': u'Inici',
    })


def styleguide(request):
    from django import forms

    choice_list = (('', 'Select…'), ('foo', 'Foo'), ('bar', 'Bar'), ('baz', 'Baz'), ('qux', 'Qux'))

    class Form(forms.Form):
        name = forms.CharField(label='Nombre', max_length=100)
        email = forms.EmailField(label='Email', required=False, help_text='This is a help text.')
        boolean = forms.BooleanField(label='Boolean', required=False)
        accept = forms.BooleanField(label='Boolean')
        choices = forms.ChoiceField(label='Choices', choices=choice_list)
        radiuses = forms.ChoiceField(label='Radius', choices=choice_list, widget=forms.RadioSelect)
        multiple = forms.MultipleChoiceField(label='Multiple Choices', choices=choice_list, initial=('foo', 'baz'))
        multicheck = forms.MultipleChoiceField(label='Checkboxes', choices=choice_list, widget=forms.CheckboxSelectMultiple)
        date = forms.DateField(label='Date', widget=forms.DateInput, initial='2015-05-01')
        time = forms.TimeField(label='Time', widget=forms.TimeInput, initial='15:20')
        datetime = forms.DateTimeField(label='Datetime', widget=forms.SplitDateTimeWidget)

    messages.info(request, 'Three credits remain in your account.')
    messages.success(request, 'Profile details updated.')
    messages.warning(request, 'Your account expires in three days.')
    messages.error(request, 'Document deleted.')

    return render(request, 'inici/styleguide.html', {
        'forms': {
            'A':               Form(),
            'B (POST Errors)': Form(request.POST)
        },
        'page_title': u'Style Guide',
    })
