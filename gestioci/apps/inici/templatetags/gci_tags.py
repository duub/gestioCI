from django import template
from django.contrib.auth.models import User
from django.forms import fields
from gestioci import formats
from gestioci.settings import auth_groups

register = template.Library()


@register.inclusion_tag('tags/form_field.html')
def form_field(field, hide_label=False):
    field.field.widget.attrs.update({"data-test-field": field.label})

    return {
        'field': field,
        'hide_label': hide_label
    }


@register.filter(name="format_data_i_hora")
def format_data_i_hora(datetime):
    return formats.format_data_i_hora(datetime)


@register.filter(name="format_data")
def format_data(date):
    return formats.format_data(date)


@register.filter(name="format_data_i_hora_curta")
def format_data_i_hora(datetime):
    return formats.format_data_i_hora(datetime, curt=True)


@register.filter(name="format_data_curta")
def format_data(date):
    return formats.format_data(date, curt=True)


@register.filter(name="format_data_i_hora_ultra_curta")
def format_data_i_hora(datetime):
    return formats.format_data_i_hora(datetime, curt=True, amagar_any_actual=True)


@register.filter(name="format_data_ultra_curta")
def format_data(date):
    return formats.format_data(date, curt=True, amagar_any_actual=True)


@register.filter(name='is_checkbox')
def is_checkbox(value):
    return isinstance(value, fields.CheckboxInput)


@register.filter(name='is_date')
def is_date(value):
    return isinstance(value, fields.DateInput)


@register.filter(name='is_datetime')
def is_datetime(value):
    return isinstance(value, fields.SplitDateTimeWidget)


@register.filter(name='is_time')
def is_time(value):
    return isinstance(value, fields.TimeInput)


@register.inclusion_tag('tags/non_field_errors.html')
def non_field_errors(form):
    return {'form': form}


# only AUTH MEMBERSHIP CHECKS beyond this point ################################

def _is_member(user, group_name):
    """
    Returns True if the given User object is a member of a group with the given name.
    Please choose the names from the gestioci.settings.auth_groups constants.
    :type group_name: basestring
    :type user: django.contrib.auth.User
    """
    if isinstance(user, User):
        return user.groups.filter(name=group_name).exists()

    return False


# no volem noms de grups "literals" als templates
# @register.filter(name="is_member_of")
# def is_member_of(u, group):
#     raise DeprecationWarning
#     return u.groups.filter(name=group).exists()

@register.filter(name='is_responsable_alta')
def is_responsable_alta(u):
    return _is_member(u, auth_groups.RESPONSABLES_ALTA)

@register.filter(name='is_exportador')
def is_exportador(u):
    return _is_member(u, auth_groups.EXPORTADORES)
