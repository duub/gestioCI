# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('socies', '0005_auto_20150629_1815'),
        ('alta_socies', '0012_auto_20150629_1815'),
        ('empreses', '0002_auto_20150629_1815'),
    ]

    operations = [
        migrations.CreateModel(
            name='FotoProjecteAutoocupatFiraire',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nom', models.CharField(max_length=256)),
                ('detalls', models.TextField(null=True, blank=True)),
                ('imatge', models.ImageField(upload_to=b'fotos_projecte_autoocupat_firaire')),
                ('url_miniatura', models.CharField(max_length=1024, null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='PolissaAsseguranca',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('comentaris_avaluacio', models.TextField(null=True)),
                ('numero_polissa', models.CharField(max_length=128, verbose_name='n\xfamero de p\xf2lissa')),
                ('data_inici_polissa', models.DateField(null=True, verbose_name="data d'inici de la cobertura")),
                ('data_final_polissa', models.DateField(null=True, verbose_name="data d'acabament de la cobertura")),
                ('import_polissa', models.DecimalField(null=True, verbose_name='import total periode cobertura', max_digits=8, decimal_places=2)),
                ('comentaris_alta', models.TextField(null=True)),
                ('activitat', models.ForeignKey(to='socies.Activitat', null=True)),
                ('adreca', models.ForeignKey(to='socies.AdrecaProjecteAutoocupat', null=True)),
                ('companyia_asseguradora', models.ForeignKey(to='empreses.EmpresaAsseguradora', null=True)),
                ('peticio', models.OneToOneField(null=True, on_delete=django.db.models.deletion.SET_NULL, to='alta_socies.PeticioAsseguranca')),
            ],
            options={
                'verbose_name': "p\xf2lissa d'asseguran\xe7a",
                'verbose_name_plural': "p\xf2lisses d'asseguran\xe7a",
            },
        ),
        migrations.CreateModel(
            name='ProjecteAutoocupat',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nom', models.CharField(max_length=256)),
                ('email', models.EmailField(max_length=254)),
                ('telefon', models.CharField(max_length=32)),
                ('website', models.URLField(null=True, verbose_name='lloc web')),
                ('descripcio', models.TextField(null=True, verbose_name='descripci\xf3')),
                ('data_creacio', models.DateField(null=True)),
                ('tipus', models.CharField(max_length=20)),
                ('individual', models.CharField(max_length=16)),
                ('es_paic', models.BooleanField(default=False)),
                ('te_comerc_electronic', models.BooleanField(default=False)),
                ('mercat_virtual', models.BooleanField(default=False)),
                ('te_productes_ecologics', models.BooleanField(default=False)),
                ('tipus_parada_firaire', models.CharField(max_length=16)),
                ('expositors', models.BooleanField(default=False)),
                ('compte_ces_assignat', models.CharField(max_length=16)),
                ('iva_assignat', models.IntegerField(default=18)),
                ('quota_avancada_quantitat', models.DecimalField(null=True, max_digits=8, decimal_places=2)),
                ('quota_avancada_forma_pagament', models.CharField(max_length=6)),
                ('adreces', models.ManyToManyField(related_name='xxx_projecte_autoocupat', to='socies.AdrecaProjecteAutoocupat')),
                ('altres_activitats', models.ManyToManyField(help_text='Altres activitats no vinculades a cap adre\xe7a', related_name='+', to='socies.Activitat')),
                ('cooperativa_assignada', models.ForeignKey(to='empreses.Cooperativa', null=True)),
                ('membres_de_referencia', models.ManyToManyField(related_name='projectes_autoocupats', verbose_name='membres de refer\xe8ncia', to='socies.Persona')),
                ('proces', models.OneToOneField(null=True, on_delete=django.db.models.deletion.SET_NULL, to='alta_socies.ProcesAltaAutoocupat')),
                ('socia_mentora', models.OneToOneField(related_name='+', null=True, to='socies.Persona')),
                ('socies_afins_addicionals', models.ManyToManyField(related_name='+', to='socies.Persona')),
            ],
            options={
                'verbose_name': 'projecte autoocupat',
                'verbose_name_plural': 'projectes autoocupats',
            },
        ),
        migrations.AddField(
            model_name='polissaasseguranca',
            name='projecte_autoocupat',
            field=models.ForeignKey(related_name='polisses_asseguranca', to='projectes.ProjecteAutoocupat'),
        ),
        migrations.AddField(
            model_name='fotoprojecteautoocupatfiraire',
            name='projecte',
            field=models.ForeignKey(related_name='fotos_parada_firaire', to='projectes.ProjecteAutoocupat'),
        ),
    ]
