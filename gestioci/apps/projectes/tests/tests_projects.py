from django.test import TestCase
from gestioci.apps.alta_socies.models import ProcesAltaAutoocupat, \
                                             SessioAcollida, \
                                             Ubicacio, PeticioAsseguranca
from gestioci.apps.socies.models import Persona, \
                                        AdrecaProjecteAutoocupat, \
                                        Activitat
from gestioci.apps.empreses.models import Cooperativa, EmpresaAsseguradora
from gestioci.apps.projectes.models import ProjecteAutoocupat, \
                                           PolissaAsseguranca
from django.utils import timezone
from django.contrib.auth.models import User


class ProjecteTestCase(TestCase):

    # fake data
    nom = "Test Project"
    email = "test@test.com"
    telefono = "66666666"
    website = "www.test.net"
    descripcio = "this is a test"
    data_resolucio = timezone.localtime(timezone.now())
    tipus = "autoocupat"
    individual = "individual"
    es_paic = False
    te_comerc_electronic = False
    mercat_virtual = False
    te_productes_ecologics = False
    tipus_parada_firaire = "no"
    expositors = False
    compte_ces_assignat = "COOPTEST"
    iva_assignat = 18

    def setUp(self):
        """
        test enviroment setup
        """

        self.activitat = Activitat.objects.create(
            nom="tostadoras"
            )

        self.persona = Persona.objects.create(
            nom=u"James",
            cognom1=u"Tont",
            cognom2=u"O",
            pseudonim=u"Es"
            )

        self.user = User.objects.create(
            username='bond',
            email='james@bond.net',
            password='top_secret')

        self.ubicacio = Ubicacio.objects.create(
            nom="test-place",
            pais="test-land",
            provincia="test-province",
            comarca="test-comarca",
            poblacio="test-town",
            codi_postal="7357",
            adreca="test-street"
            )

        # It is the only session that needs to be non-null
        SessioAcollida.objects.create(
            data=self.data_resolucio,
            duracio=25,
            responsable_alta=self.user,
            ubicacio=self.ubicacio
            )

        self.adreca = AdrecaProjecteAutoocupat.objects.create(
            pais=self.ubicacio.pais,
            provincia=self.ubicacio.provincia,
            comarca=self.ubicacio.comarca,
            poblacio=self.ubicacio.poblacio,
            codi_postal=self.ubicacio.codi_postal,
            adreca=self.ubicacio.adreca,
            designacio="testwhere",
            traspas="cap")

        self.cooperativa = Cooperativa.objects.create(
            nom="Test-ACME",
            nom_fiscal="Test-ACME coop.",
            nif="Test-NIF")

        self.asseguradora = EmpresaAsseguradora.objects.create(
            nom="Ass and gura Dora",
            nom_fiscal="Test-Empresa",
            nif="ACME_NIF")

        self.proces_star = ProcesAltaAutoocupat.objects.create(
            nom=self.nom,
            email=self.email,
            telefon=self.telefono,
            website=self.website,
            descripcio=self.descripcio,
            data_resolucio=self.data_resolucio,
            tipus=self.tipus,
            individual=self.individual,
            es_paic=self.es_paic,
            te_comerc_electronic=self.te_comerc_electronic,
            mercat_virtual=self.mercat_virtual,
            te_productes_ecologics=self.te_productes_ecologics,
            tipus_parada_firaire=self.tipus_parada_firaire,
            expositors=self.expositors,
            compte_CES_assignat=self.compte_ces_assignat,
            iva_assignat=self.iva_assignat,
            sessio_acollida=SessioAcollida.objects.get(data=self.data_resolucio),
            responsable=self.user,
            cooperativa_assignada=self.cooperativa,
            socia_mentora=self.persona
            )

        self.proces_star.membres_de_referencia.add(self.persona)
        self.proces_star.adreces.add(self.adreca)

        self.projecte = ProjecteAutoocupat.objects.create_projecte(proces=self.proces_star)

        self.peticio = PeticioAsseguranca.objects.create(
            adreca=self.adreca,
            activitat=self.activitat,
            proces_alta_autoocupat=self.proces_star,
            comentaris_avaluacio="bla bla bla...",
            companyia_asseguradora=self.asseguradora,
            numero_polissa="1920301",
            data_inici_polissa=self.data_resolucio,
            data_final_polissa=self.data_resolucio,
            import_polissa=100.12,
            )

        self.polissa = PolissaAsseguranca.objects.create_polissa(
            peticio=self.peticio,
            projecte=self.projecte)

    def test_project_consistency_simple(self):
        """
        this tests every field excluding foreign keys and
        many to many relationships
        """

        self.assertEqual(self.proces_star.nom, self.projecte.nom)
        self.assertEqual(self.proces_star.email, self.projecte.email)
        self.assertEqual(self.proces_star.telefon, self.projecte.telefon)
        self.assertEqual(self.proces_star.website, self.projecte.website)
        self.assertEqual(self.proces_star.descripcio, self.projecte.descripcio)
        self.assertEqual(self.proces_star.data_resolucio, self.projecte.data_creacio)
        self.assertEqual(self.proces_star.tipus, self.projecte.tipus)
        self.assertEqual(self.proces_star.individual, self.projecte.individual)
        self.assertEqual(self.proces_star.es_paic, self.projecte.es_paic)
        self.assertEqual(self.proces_star.te_comerc_electronic, self.projecte.te_comerc_electronic)
        self.assertEqual(self.proces_star.mercat_virtual, self.projecte.mercat_virtual)
        self.assertEqual(self.proces_star.te_productes_ecologics, self.projecte.te_productes_ecologics)
        self.assertEqual(self.proces_star.tipus_parada_firaire, self.projecte.tipus_parada_firaire)
        self.assertEqual(self.proces_star.expositors, self.projecte.expositors)
        self.assertEqual(self.proces_star.compte_CES_assignat, self.projecte.compte_ces_assignat)
        self.assertEqual(self.proces_star.iva_assignat, self.projecte.iva_assignat)
    
    def test_project_consistency_foreign(self):
        """
        this tests every foreign key
        """

        self.assertEqual(self.proces_star.cooperativa_assignada, self.projecte.cooperativa_assignada)

    def test_project_consistency_many(self):
        """
        this tests every many to many relationship
        """

        self.assertQuerysetEqual(self.proces_star.adreces.all(), map(repr, self.projecte.adreces.all()))
        self.assertQuerysetEqual(self.proces_star.membres_de_referencia.all(), map(repr, self.projecte.membres_de_referencia.all()))

    def test_project_consistency_one(self):
        """
        this tests every one to one relationship
        """

        self.assertEqual(self.proces_star.socia_mentora, self.projecte.socia_mentora)

    def test_project_on_delete_o2o(self):
        """
        this tests the consistency of the model
        when the proces is deleted
        """

        self.proces_star.delete()

        self.assertEqual(self.projecte.nom, self.nom)
        self.assertNotEqual(self.projecte.proces, None)

    def test_polissa_consistency(self):
        """
        this tests the consistency of Polissa
        """

        self.assertEqual(self.peticio.activitat, self.polissa.activitat)
        self.assertEqual(self.peticio.adreca, self.polissa.adreca)
        self.assertEqual(self.peticio.comentaris_avaluacio, self.polissa.comentaris_avaluacio)
        self.assertEqual(self.peticio.companyia_asseguradora, self.polissa.companyia_asseguradora)
        self.assertEqual(self.peticio.numero_polissa, self.polissa.numero_polissa)
        self.assertEqual(self.peticio.data_inici_polissa, self.polissa.data_inici_polissa)
        self.assertEqual(self.peticio.data_final_polissa, self.polissa.data_final_polissa)
        self.assertEqual(self.peticio.import_polissa, self.polissa.import_polissa)
