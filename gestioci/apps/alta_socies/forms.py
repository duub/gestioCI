# coding=utf-8
from datetime import timedelta

from django import forms
from django.core.exceptions import ValidationError
from django.contrib.auth.models import Group
from django.utils import timezone

from .models import Ubicacio, SessioAcollida, SessioAvaluacio, SessioMoneda, SessioAlta, ProcesAltaAutoocupat, \
    PeticioConcessioActivitat, PeticioAsseguranca, FotoProjecteAutoocupatFiraire
from gestioci.apps.socies.helpers import es_dni_valid
from gestioci.apps.socies.models import Activitat, AdrecaProjecteAutoocupat, ContribucioCIC, GrupHabilitat, Persona


class FormulariAssistenciaAcollida(forms.ModelForm):

    class Meta:
        model = ProcesAltaAutoocupat
        fields = ['nom', 'email', 'telefon', ]


class FormulariAltaProjecteAutoocupatSessioMoneda(forms.ModelForm):

    class Meta:
        model = ProcesAltaAutoocupat
        fields = ['sessio_moneda']

    def __init__(self, *args, **kwargs):
        super(FormulariAltaProjecteAutoocupatSessioMoneda, self).__init__(*args, **kwargs)

        self.fields['sessio_moneda'].required = True


class FormulariAltaProjecteAutoocupatCessioUs(forms.ModelForm):

    class Meta:
        model = AdrecaProjecteAutoocupat
        fields = ['traspas_inici', 'traspas_final', 'traspas_comentaris_alta']

    def __init__(self, *args, **kwargs):
        super(FormulariAltaProjecteAutoocupatCessioUs, self).__init__(*args, **kwargs)

        self.fields['traspas_inici'].required = True
        self.fields['traspas_final'].required = True


class FormulariAltaProjecteAutoocupatLloguer(forms.ModelForm):

    class Meta:
        model = AdrecaProjecteAutoocupat
        fields = ['traspas_inici', 'traspas_final', 'traspas_comentaris_alta',
                  'traspas_lloguer_import_mensual']

    def __init__(self, *args, **kwargs):
        super(FormulariAltaProjecteAutoocupatLloguer, self).__init__(*args, **kwargs)

        self.fields['traspas_inici'].required = True
        self.fields['traspas_final'].required = True
        self.fields['traspas_lloguer_import_mensual'].required = True


# TODO aquest nom s'hauria de canviar; ja ho avaluare quan fem les altes "d'altres socis"
class FormulariProjecte(forms.ModelForm):

    class Meta:
        model = ProcesAltaAutoocupat
        fields = ['sessio_acollida', 'nom', 'email', 'telefon', ]

    def clean(self):

        cleaned_data = super(FormulariProjecte, self).clean()

        nom = cleaned_data.get('nom')
        if nom:
            cleaned_data['nom'] = nom.strip()
        # TODO clean also email and telefon?

        if ProcesAltaAutoocupat.objects.filter(nom__iexact=nom).exclude(pk=self.instance.pk).exists():
            self.add_error('nom', "Ja hi ha un projecte amb aquest nom!")

        return cleaned_data


# TODO aquest nom s'hauria de canviar; ja ho avaluare quan fem les altes "d'altres socis"
class FormulariDadesGeneralsProjecte(forms.ModelForm):

    class Meta:
        model = ProcesAltaAutoocupat
        fields = ['descripcio',
                  'website',
                  'tipus',
                  'individual',
                  'te_comerc_electronic',
                  'te_productes_ecologics',
                  'tipus_parada_firaire',
                  'expositors']

    def __init__(self, *args, **kwargs):
        super(FormulariDadesGeneralsProjecte, self).__init__(*args, **kwargs)

        self.fields['tipus'].required = True
        self.fields['tipus'].widget = forms.RadioSelect(choices=ProcesAltaAutoocupat.TIPUS)

        self.fields['individual'].required = True
        self.fields['individual'].widget = forms.RadioSelect(choices=ProcesAltaAutoocupat.INDIVIDUAL)

        self.fields['descripcio'].widget = forms.Textarea(attrs={'class': 'descripcio_field',
                                                                 'rows': 15})
        self.fields['website'].widget = forms.URLInput(attrs={'class': 'url_field'})
        self.fields['tipus_parada_firaire'].widget = forms.RadioSelect(choices=ProcesAltaAutoocupat.PARADA_FIRAIRE)

        self.fields['descripcio'].required = True


class FormulariDetallsMembreDeReferencia(forms.Form):

    # una mica cutre; s'ha de mantenir "sincronitzat" amb socies.Persona
    id = forms.IntegerField()
    nom_sencer = forms.CharField(max_length=360)
    email = forms.EmailField(label=u"Correu electrònic")
    tipus_document_dni = forms.CharField(max_length=16)
    dni = forms.CharField(max_length=64)
    compte_integralces = forms.CharField(max_length=16, required=False,
                                         label=u"IntegralCES", help_text=u"Número de compte a IntegralCES")
    contribucions = forms.MultipleChoiceField(required=False,
                                              widget=forms.CheckboxSelectMultiple,
                                              label=u"Tipus de contribució a la CIC")
    habilitats = forms.MultipleChoiceField(required=False, label=u"Habilitats, coneixements i experiència")
    detalls_habilitats = forms.CharField(max_length=1024, required=False,
                                         widget=forms.Textarea,
                                         label=u"Detalls sobre habilitats, coneixements i experiència")

    def __init__(self, *args, **kwargs):
        super(FormulariDetallsMembreDeReferencia, self).__init__(*args, **kwargs)
        self.fields['id'].widget = forms.HiddenInput()
        self.fields['nom_sencer'].widget = forms.HiddenInput()
        self.fields['tipus_document_dni'].widget = forms.Select(choices=Persona.TIPUS_DOCUMENT_ID)
        self.fields['contribucions'].choices = ContribucioCIC.objects.values_list('id', 'nom')
        self.fields['contribucions'].widget.attrs = {'size': min(len(self.fields['contribucions'].choices), 5)}
        self.fields['dni'].widget.attrs = {'data-dni': ''}

        habilitats_choices = []
        for hg in GrupHabilitat.objects.all():
            habilitats_choices.append(
                (
                    hg.nom,
                    tuple(hg.habilitat_set.values_list('id', 'nom'))
                )
            )
        self.fields['habilitats'].choices = habilitats_choices
        self.fields['habilitats'].widget.attrs = {'size': 15}
        self.fields['detalls_habilitats'].widget.attrs = {'class': 'detalls_habilitats'}

    def clean(self):

        cleaned_data = super(FormulariDetallsMembreDeReferencia, self).clean()

        dni = cleaned_data.get('dni')
        if dni:
            dni = dni.strip()
            cleaned_data['dni'] = dni

            tipus = cleaned_data.get('tipus_document_dni')
            if tipus == Persona.TIPUS_DOCUMENT_ID_DNI:
                if not es_dni_valid(dni):
                    self.add_error('dni', u"Això no és un DNI vàlid")

        return cleaned_data


# TODO aquest formulari potser hauria de dir-se FormulariGraellaPersona, i derivar per herència un amb el nom actual
class FormulariMembreDeReferencia(forms.Form):
    """
    Hem de mantenir aquesta llista de camps i propietats
    sincronitzada amb la definicio de Persona!
    """

    _extra_attributes = {'onchange': 'camp_persona_modificat(event.target);',
                         'onfocus':  'canviar_de_fila(event.target);',
                         'onblur':   'canviar_de_fila(event.target);'}

    id = forms.IntegerField(required=False, widget=forms.HiddenInput)
    nom = forms.CharField(max_length=64, required=False, widget=forms.TextInput(attrs=_extra_attributes))
    cognom1 = forms.CharField(max_length=64, required=False, widget=forms.TextInput(attrs=_extra_attributes))
    cognom2 = forms.CharField(max_length=64, required=False, widget=forms.TextInput(attrs=_extra_attributes))
    # pseudonim = forms.CharField(max_length=64, required=False, widget=forms.TextInput(attrs=_extra_attributes))
    # email = forms.EmailField(required=False, widget=forms.TextInput(attrs=_extra_attributes))
    telefon = forms.CharField(max_length=64, required=False, widget=forms.TextInput(attrs=_extra_attributes))


class FormulariAdrecaProjecteAutoocupat(forms.ModelForm):

    class Meta:
        model = AdrecaProjecteAutoocupat
        fields = '__all__'
        widgets = {
            'traspas': forms.RadioSelect(),
            'activitats': forms.CheckboxSelectMultiple()
        }


class FormulariCercaActualitzacioSeleccioPersones(forms.ModelForm):

    id_persona = forms.IntegerField(required=False)
    persones_escollides = forms.CharField(max_length=1024, required=False)

    class Meta:
        model = Persona
        fields = ['nom', 'cognom1', 'cognom2', 'pseudonim', 'email',
                  'telefon', 'compte_integralces', 'tipus_document_dni', 'dni']

    def __init__(self, *args, **kwargs):

        # TODO aquesta merda dels camps obligatoris s'ha de refer amb herència del formulari
        camps_obligatoris = []
        if 'camps_obligatoris' in kwargs.keys():
            camps_obligatoris = kwargs.get('camps_obligatoris')
            del(kwargs['camps_obligatoris'])

        super(FormulariCercaActualitzacioSeleccioPersones, self).__init__(*args, **kwargs)

        self.fields['dni'].widget.attrs = {'autocomplete': 'off',
                                           'data-dni': ''}

        self.fields['id_persona'].widget = forms.HiddenInput()
        self.fields['persones_escollides'].widget = forms.HiddenInput()

        for camp in camps_obligatoris:
            assert(camp in self.Meta.fields)
            self.fields[camp].required = True

        for camp in self.Meta.fields:
            if camp != 'dni':
                self.fields[camp].widget.attrs = {'onkeydown': 'return capturar_tecla_enter(event);',
                                                  'autocomplete': 'off'}


class ForulariPas19(forms.ModelForm):

    class Meta:
        model = ProcesAltaAutoocupat
        fields = ['compte_CES_assignat']

    def __init__(self, *args, **kwargs):

        super(ForulariPas19, self).__init__(*args, **kwargs)

        self.fields['compte_CES_assignat'].required = True


class FormulariFotoProjecteAutoocupatFiraire(forms.ModelForm):

    imatges_a_eliminar = forms.CharField(max_length=1024, required=False, widget=forms.HiddenInput)

    class Meta:
        model = FotoProjecteAutoocupatFiraire
        fields = ['nom', 'detalls', 'imatge']

    def __init__(self, *args, **kwargs):
        super(FormulariFotoProjecteAutoocupatFiraire, self).__init__(*args, **kwargs)
        self.fields['detalls'].widget = forms.Textarea(attrs={'rows': 3})


class FormulariAltresActivitatsProjecteAutoocupatFiraire(forms.ModelForm):

    class Meta:
        model = ProcesAltaAutoocupat
        fields = ['altres_activitats']
        widgets = {'altres_activitats': forms.CheckboxSelectMultiple()}


class FormulariAltresActivitatsProjecteAutoocupatNoFiraire(FormulariAltresActivitatsProjecteAutoocupatFiraire):

    def __init__(self, *args, **kwargs):
        super(FormulariAltresActivitatsProjecteAutoocupatNoFiraire, self).__init__(*args, **kwargs)
        self.fields['altres_activitats'].queryset = Activitat.objects.filter(firaire=False)


class FormulariFormaPagamentQuotesAutoocupat(forms.ModelForm):

    class Meta:
        model = ProcesAltaAutoocupat
        fields = ['quota_alta_forma_pagament', 'quota_avancada_forma_pagament']

    def __init__(self, *args, **kwargs):
        super(FormulariFormaPagamentQuotesAutoocupat, self).__init__(*args, **kwargs)

        proces = kwargs.get('instance')

        opcions = proces.get_opcions_quota_display(proces.QUOTA_ALTA)
        opcions = ((k, v) for k, v in opcions.iteritems())
        self.fields['quota_alta_forma_pagament'].widget = forms.RadioSelect(choices=opcions)

        opcions = proces.get_opcions_quota_display(proces.QUOTA_TRIMESTRAL_AVANCADA)
        opcions = ((k, v) for k, v in opcions.iteritems())
        self.fields['quota_avancada_forma_pagament'].widget = forms.RadioSelect(choices=opcions)


class FormulariAltaConcessioActivitat(forms.ModelForm):

    class Meta:
        model = PeticioConcessioActivitat
        fields = ['numero_llicencia',
                  'data_inici_llicencia',
                  'data_final_llicencia',
                  'data_inici_concessio',
                  'data_final_concessio',
                  'comentaris_alta']

    def __init__(self, *args, **kwargs):
        super(FormulariAltaConcessioActivitat, self).__init__(*args, **kwargs)

        self.fields['numero_llicencia'].required = True
        self.fields['data_inici_llicencia'].required = True
        self.fields['data_final_llicencia'].required = False
        self.fields['data_inici_concessio'].required = True
        self.fields['data_final_concessio'].required = False
        self.fields['comentaris_alta'].widget = forms.Textarea(attrs={'rows': 3})


class FormulariAltaAsseguranca(forms.ModelForm):

    class Meta:
        model = PeticioAsseguranca
        fields = ['companyia_asseguradora',
                  'numero_polissa',
                  'data_inici_polissa',
                  'data_final_polissa',
                  'import_polissa',
                  'comentaris_alta']

    def __init__(self, *args, **kwargs):
        super(FormulariAltaAsseguranca, self).__init__(*args, **kwargs)

        self.fields['companyia_asseguradora'].required = True
        self.fields['numero_polissa'].required = True
        self.fields['data_inici_polissa'].required = True
        self.fields['data_final_polissa'].required = True
        self.fields['import_polissa'].required = True
        self.fields['comentaris_alta'].widget = forms.Textarea(attrs={'rows': 3})


class FormulariAltaQuotes(forms.ModelForm):

    alta_confirmacio = forms.BooleanField(widget=forms.CheckboxInput,
                                          initial=False,
                                          label=u"He rebut el pagament")

    avancada_confirmacio = forms.BooleanField(widget=forms.CheckboxInput,
                                              initial=False,
                                              label=u"He rebut el pagament")

    class Meta:
        model = ProcesAltaAutoocupat
        fields = ['quota_alta_forma_pagament',
                  'quota_avancada_forma_pagament',
                  'compte_CES_que_paga_les_quotes',
                  # 'pagament_quotes_numero_transaccio',
                  'comentaris_pagament_quotes']

    def __init__(self, *args, **kwargs):
        super(FormulariAltaQuotes, self).__init__(*args, **kwargs)

        proces = kwargs.get('instance')

        self.fields['comentaris_pagament_quotes'].widget = forms.Textarea(attrs={'rows': 3})

        opcions = proces.get_opcions_quota_display(proces.QUOTA_ALTA)
        if proces.quota_alta_forma_pagament != proces.FORMA_PAGAMENT_HORA:
            del opcions[proces.FORMA_PAGAMENT_HORA]
        opcions = ((k, v) for k, v in opcions.iteritems())
        self.fields['quota_alta_forma_pagament'].widget = forms.RadioSelect(choices=opcions)

        opcions = proces.get_opcions_quota_display(proces.QUOTA_TRIMESTRAL_AVANCADA)
        opcions = ((k, v) for k, v in opcions.iteritems())
        self.fields['quota_avancada_forma_pagament'].widget = forms.RadioSelect(choices=opcions)

        self.fields['quota_alta_forma_pagament'].label = u"Quota d'alta"
        self.fields['quota_avancada_forma_pagament'].label = u"Quota trimestral avançada"
        self.fields['compte_CES_que_paga_les_quotes'].label = u"Compte de l'IntegralCES que paga les quotes"
        # self.fields['pagament_quotes_numero_transaccio'].label = u"Número de transacció a l'IntegralCES"

    def clean(self):

        cleaned_data = super(FormulariAltaQuotes, self).clean()

        quelcom_es_paga_en_ecos = (
            cleaned_data.get('quota_alta_forma_pagament') == ProcesAltaAutoocupat.FORMA_PAGAMENT_ECO or
            cleaned_data.get('quota_avancada_forma_pagament') == ProcesAltaAutoocupat.FORMA_PAGAMENT_ECO)

        cleaned_data['compte_CES_que_paga_les_quotes'] = cleaned_data.get('compte_CES_que_paga_les_quotes').upper()
        if quelcom_es_paga_en_ecos:
            if not cleaned_data.get('compte_CES_que_paga_les_quotes').startswith('COOP'):
                self.add_error('compte_CES_que_paga_les_quotes',
                               u"Camp obligatori si es paga alguna quota en ecos. Ha de començar per \"COOP\".")
            # per ara l'integralces no informa clarament de cap número de transacció. No ho fem obligatori, doncs.
            # if not cleaned_data.get('pagament_quotes_numero_transaccio'):
            #     self.add_error('pagament_quotes_numero_transaccio',
            #                    u"Copieu aquí el número de transacció que ha donat l'IntegralCES.")

        if not (cleaned_data.get('alta_confirmacio', False) and cleaned_data.get('avancada_confirmacio', False)):
            self.add_error('__all__', u"S'han de fer efectives les dues quotes!")

        return cleaned_data


class GeneradorSessions(forms.Form):

    # TODO refactor treure els tipus de sessió d'aqui :)
    # TODO keep in sync with alta_socies.views.sessions_del_mes
    TIPUS_SESSIO_AVALUACIO = 'avaluacio'
    TIPUS_SESSIO_ALTA = 'alta'
    TIPUS_DE_SESSIONS = (
        (TIPUS_SESSIO_AVALUACIO, "avaluació"),
        (TIPUS_SESSIO_ALTA, "alta"),
    )

    tipus_sessio = forms.ChoiceField(
        choices=TIPUS_DE_SESSIONS,
        widget=forms.RadioSelect)

    responsables_alta = forms.ModelMultipleChoiceField(
        queryset=None,
        widget=forms.CheckboxSelectMultiple)

    data_i_hora_inici = forms.DateTimeField(
        label="Data i hora d'inici de la primera sessió",
        widget=forms.SplitDateTimeWidget(time_format='%H:%M'))

    duracio = forms.IntegerField(
        label="duració de cada sessió",
        help_text="minuts",
        min_value=15,
        max_value=300)

    num_sessions = forms.IntegerField(
        label="Número de sessions",
        min_value=1,
        max_value=60)

    ubicacio = forms.ModelChoiceField(
        label="Ubicació",
        queryset=None)

    assabentada_del_risc = forms.BooleanField(
        label="Entenc el risc",
        help_text="ATENCIÓ! aquesta eina no comprova si les sessions que crearà "
                  "es solapen amb altres ja donades d'alta, ni té en compte la "
                  "disponibilitat dels responsables d'alta, espais o recursos necessaris!")

    def __init__(self, *args, **kwargs):
        super(GeneradorSessions, self).__init__(*args, **kwargs)

        responsables_alta = Group.objects.get(name='''Responsables d'alta''').user_set.all()
        ubicacions = Ubicacio.objects.all()

        self.fields['responsables_alta'].queryset = responsables_alta
        self.fields['ubicacio'].queryset = ubicacions

    def clean(self):

        cleaned_data = super(GeneradorSessions, self).clean()

        inici = cleaned_data.get('data_i_hora_inici')
        duracio = cleaned_data.get('duracio')
        num_sessions = cleaned_data.get('num_sessions')
        if inici and duracio and num_sessions:
            final = inici + timedelta(minutes=duracio * num_sessions, seconds=-1)
            if inici.day != final.day or inici.month != final.month or inici.year != final.year:
                raise ValidationError("L'última sessió ha d'acabar abans de mitjanit")
