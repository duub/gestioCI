# coding=utf-8
import calendar
import json
from datetime import date, timedelta

from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView
from django.contrib.messages.views import SuccessMessageMixin
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.core.exceptions import SuspiciousOperation
from django.forms import modelformset_factory, formset_factory, SplitDateTimeWidget
from django.forms.models import modelform_factory
from django.http import Http404
from django.shortcuts import render, get_object_or_404, redirect
from django.template import RequestContext
from django.utils import timezone
from django.conf import settings
from django.db.models import Count
from django.http import HttpResponse
from django.template import loader, Context
from django.contrib.auth.models import User

from .forms import GeneradorSessions, FormulariProjecte, FormulariDadesGeneralsProjecte, \
    FormulariDetallsMembreDeReferencia, FormulariAdrecaProjecteAutoocupat, \
    FormulariAltaProjecteAutoocupatSessioMoneda, FormulariAltaProjecteAutoocupatCessioUs, \
    FormulariAltaProjecteAutoocupatLloguer, FormulariAltresActivitatsProjecteAutoocupatFiraire, \
    FormulariAltresActivitatsProjecteAutoocupatNoFiraire, FormulariAltaConcessioActivitat, FormulariAltaAsseguranca, \
    FormulariAltaQuotes, FormulariFormaPagamentQuotesAutoocupat, FormulariCercaActualitzacioSeleccioPersones, \
    ForulariPas19, FormulariFotoProjecteAutoocupatFiraire
from gestioci.settings import auth_groups
from .models import SessioAlta, SessioAvaluacio, ProcesAltaAutoocupat, SessioAcollida, \
    PeticioConcessioActivitat, PeticioAsseguranca, SessioMoneda
from gestioci.apps.socies.models import Persona, AdrecaProjecteAutoocupat, HabilitatsContribucionsPersona, \
    Habilitat, ContribucioCIC, Activitat
from gestioci.apps.empreses.models import Cooperativa
from gestioci.formats import dow_names
from gestioci.thumbnails import create_thumbnail
from gestioci.apps.projectes.models import FotoProjecteAutoocupatFiraire, \
    ProjecteAutoocupat, PolissaAsseguranca
from gestioci.apps.alta_socies.utils import generate_task_list_pdf

@login_required
def rebutjar_proces_alta_autoocupat(request, proces_id):
    """
    Vista para rechazar un proceso de alta para
    un socio autoocupado
    """
    proces = ProcesAltaAutoocupat.objects.get(id=proces_id)
    proces.resolucio = 'rebutjat'
    proces.data_resolucio = timezone.localtime(timezone.now())
    proces.save()
    messages.error(request, "El projecte ha sigut rebutjat correctament")
    return redirect('inici:index')


def is_user_responsable_alta(user):
    """
    function that returns True when user belongs
    to RESPONSABLE_ALTA group
    """
    return user.groups.filter(name=auth_groups.RESPONSABLES_ALTA).exists()


class FilterResponsablesAltaMixin(object):
    """
    Mixin per filtrar la llista d'opcions al widget del camp "responsable_alta"
    de manera que només ofereix usuaris membres del grup RESPONSABLES_ALTA
    """
    def get_context_data(self, **kwargs):
        context = super(FilterResponsablesAltaMixin, self).get_context_data(**kwargs)
        context['form'].fields['responsable_alta'].queryset = User.objects.filter(
            groups__name=auth_groups.RESPONSABLES_ALTA)
        return context


def is_user_exporter(user):
    """
    function that returns True when user belongs
    to EXPORTADORES group
    """
    return user.groups.filter(name=auth_groups.EXPORTADORES).exists()


@login_required
@user_passes_test(is_user_exporter)
def exportar_a_csv(request):
    """ 
    Vista per exportar els processos d'alta de projecte
    autoocupat en format CSV ("cap al transversal")
    """

    d = timezone.localtime(timezone.now()).date().isoformat()
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="projectes-%s.csv"' % d

    queryset = ProcesAltaAutoocupat.objects.all()

    csv_data = []
    for p in queryset:
        projecte = []
        projecte += [
            p.data_resolucio,
            p.compte_CES_assignat,
            p.cooperativa_assignada,
            p.email,
            p.telefon]
        if p.adreces.first() is not None:
            projecte += [
                p.adreces.first().adreca,
                p.adreces.first().codi_postal,
                p.adreces.first().poblacio,
                p.adreces.first().comarca]
        else:
            projecte += [
                None,
                None,
                None,
                None]
        if p.membres_de_referencia.first() is not None:
            projecte += [
                p.membres_de_referencia.first().nom,
                p.membres_de_referencia.first().cognom1 +
                " " + p.membres_de_referencia.first().cognom2,
                p.membres_de_referencia.first().dni]
        else:
            projecte += [
                None,
                None,
                None]
        projecte.append(p.individual)
        projecte.append(p.pas)
        projecte = ['' if word is None else word for word in projecte]
        csv_data.append(projecte)
    """
    # How it should be when all projects are accepted
    csv_data = [[p.data_resolucio,
                 p.compte_CES_assignat,
                 p.compte_CES_assignat,
                 p.cooperativa_assignada,
                 p.email,
                 p.telefon,
                 p.adreces.first().adreca,
                 p.adreces.first().codi_postal,
                 p.adreces.first().poblacio,
                 p.adreces.first().comarca,
                 p.membres_de_referencia.objects.first().nom,
                 p.membres_de_referencia.objects.first().cognom1 +
                 " " + p.membres_de_referencia.objects.first().cognom2,
                 p.membres_de_referencia.objects.first().dni,
                 p.individual]
                for p in queryset]
    """
    t = loader.get_template("alta_socies/export.txt")
    c = Context({'data': csv_data})
    response.write(t.render(c))
    return response


class ProcesAltaAutoocupatDetailView(DetailView):
    """Vista resum per visualitzar els detalls
       d'un procés d'alta quan no es vol editar,
       o quan no es té permís per editar-lo
       p. ex. quan el vol consultar algú que no
       n'és el responsable d'alta"""

    model = ProcesAltaAutoocupat
    context_object_name = 'proces'
    template_name = 'alta_socies/autoocupat/detail.html'

    @method_decorator(login_required)
    @method_decorator(user_passes_test(is_user_responsable_alta))
    def dispatch(self, *args, **kwargs):
        return super(ProcesAltaAutoocupatDetailView, self).dispatch(*args, **kwargs)


class SessioAcollidaUpdateView(SuccessMessageMixin, FilterResponsablesAltaMixin, UpdateView):
    """Vista per editar les sessions d'acollida"""

    model = SessioAcollida
    template_name = 'alta_socies/formulari_sessio.html'
    success_url = '/'
    success_message = "La sessió d'acollida s'ha actualitzat correctament"
    title = u"Editar sessió d'acollida"
    action_label = u"Desar"

    @method_decorator(login_required)
    @method_decorator(user_passes_test(is_user_responsable_alta))
    def dispatch(self, *args, **kwargs):
        return super(SessioAcollidaUpdateView, self).dispatch(*args, **kwargs)


class SessioAcollidaListView(ListView):
    """Vista per llistar les sessions d'acollida"""

    context_object_name = 'sessions'
    queryset = SessioAcollida.objects.order_by('-data')
    template_name = 'alta_socies/llistat_sessions.html'

    title = u"Llistat de sessions d'acollida"
    edit_url = 'alta_socies:editar_sessio_acollida'

    @method_decorator(login_required)
    @method_decorator(user_passes_test(is_user_responsable_alta))
    def dispatch(self, *args, **kwargs):
        return super(SessioAcollidaListView, self).dispatch(*args, **kwargs)


class SessioAcollidaCreateView(SuccessMessageMixin, FilterResponsablesAltaMixin, CreateView):
    """Vista per crear sessions d'acollida"""

    model = SessioAcollida
    template_name = 'alta_socies/formulari_sessio.html'
    form_class = modelform_factory(SessioAcollida,
                                   widgets={"data": SplitDateTimeWidget},
                                   fields=['data', 'ubicacio', 'duracio', 'responsable_alta'])
    success_url = '/'
    success_message = "La sessió d'acollida ha sigut creada correctament"
    title = u"Crear sessió d'acollida"
    action_label = u"Crear"

    @method_decorator(login_required)
    @method_decorator(user_passes_test(is_user_responsable_alta))
    def dispatch(self, *args, **kwargs):
        return super(SessioAcollidaCreateView, self).dispatch(*args, **kwargs)


class SessioMonedaUpdateView(SuccessMessageMixin, FilterResponsablesAltaMixin, UpdateView):
    """Vista per editar les sessions de moneda social"""

    model = SessioMoneda
    template_name = 'alta_socies/formulari_sessio.html'
    success_url = '/'
    success_message = "La sessió de moneda social s'ha actualitzat correctament"
    title = u"Editar sessiò de moneda social"
    action_label = u"Desar"

    @method_decorator(login_required)
    @method_decorator(user_passes_test(is_user_responsable_alta))
    def dispatch(self, *args, **kwargs):
        return super(SessioMonedaUpdateView, self).dispatch(*args, **kwargs)


class SessioMonedaListView(ListView):
    """Vista per llistar les sessions de moneda social"""

    context_object_name = 'sessions'
    queryset = SessioMoneda.objects.order_by('-data')
    template_name = 'alta_socies/llistat_sessions.html'

    title = u"Llistat de sessions de moneda social"
    edit_url = 'alta_socies:editar_sessio_moneda'

    @method_decorator(login_required)
    @method_decorator(user_passes_test(is_user_responsable_alta))
    def dispatch(self, *args, **kwargs):
        return super(SessioMonedaListView, self).dispatch(*args, **kwargs)


class SessioMonedaCreateView(SuccessMessageMixin, FilterResponsablesAltaMixin, CreateView):
    """Vista per crear sessions de moneda social"""

    model = SessioMoneda
    template_name = 'alta_socies/formulari_sessio.html'
    form_class = modelform_factory(SessioMoneda,
                                   widgets={"data": SplitDateTimeWidget},
                                   fields=['data', 'ubicacio', 'duracio', 'responsable_alta'])
    success_url = '/'
    success_message = "La sessiò de moneda social ha sigut creada correctament"
    title = u"Crear sessiò de moneda social"
    action_label = u"Crear"

    @method_decorator(login_required)
    @method_decorator(user_passes_test(is_user_responsable_alta))
    def dispatch(self, *args, **kwargs):
        return super(SessioMonedaCreateView, self).dispatch(*args, **kwargs)


def cooperatiu(request):
    return render(request, 'alta_socies/cooperatiu.html', {
    })


@login_required
def generar_sessions(request):

    if request.method == 'POST':
        form = GeneradorSessions(data=request.POST)

        sessions_creades = []

        if form.is_valid():
            tipus_sessio = form.cleaned_data.get('tipus_sessio')
            responsables_alta = form.cleaned_data.get('responsables_alta')
            inici_jornada = form.cleaned_data.get('data_i_hora_inici')
            duracio = form.cleaned_data.get('duracio')
            num_sessions = form.cleaned_data.get('num_sessions')
            ubicacio = form.cleaned_data.get('ubicacio')

            # TODO començar una transacció en aquest punt per poder fer ROLLBACK si es troben sessions ja creades
            # un mínim acceptable penso que seria considerar duplicada una sessió per mateixa data i responsable_alta
            # independentment d'ubicació

            for responsable_alta in responsables_alta:

                sessions_del_responsable_alta = []

                for n in range(0, num_sessions):

                    if tipus_sessio == form.TIPUS_SESSIO_AVALUACIO:
                        sessio = SessioAvaluacio()
                    elif tipus_sessio == form.TIPUS_SESSIO_ALTA:
                        sessio = SessioAlta()
                    else:
                        raise NotImplementedError(u"Tipus de sessió desconeguda: %s" % tipus_sessio)

                    sessio.data = inici_jornada + timedelta(minutes=duracio * n)
                    sessio.ubicacio = ubicacio
                    sessio.responsable_alta = responsable_alta
                    sessio.duracio = duracio
                    sessio.save()

                    sessions_del_responsable_alta.append(sessio)

                sessions_creades.append(
                    {'responsable_alta': responsable_alta, 'sessions': sessions_del_responsable_alta})

    else:
        form = GeneradorSessions()
        sessions_creades = None

    return render(request, 'alta_socies/generador_sessions.html', {
        'form': form,
        'sessions_creades': sessions_creades,
    })


@login_required
@user_passes_test(is_user_responsable_alta)
def proces_alta_projecte_autoocupat_llista_de_tasques(request, id_proces, format='html'):

    if format not in ['html', 'pdf']:
        raise Http404(u"El format que estás demanant es sospitós")

    etiqueta_sense_comentaris = u"—"

    proces = get_object_or_404(ProcesAltaAutoocupat, pk=id_proces)

    if proces.pas < 9:
        raise Http404(u"Aquest procés d'alta de projecte autoocupat no està al pas 9!")

    # adreces amb traspas (lloguers i cessions d'ús)
    adreces_amb_traspas = proces.adreces.exclude(traspas=AdrecaProjecteAutoocupat.TIPUS_TRASPAS_CAP)
    traspassos = []
    for a in adreces_amb_traspas:
        if a.traspas == a.TIPUS_TRASPAS_LLOGUER:
            tipus = u"Lloguer"
        elif a.traspas == a.TIPUS_TRASPAS_CESSIO:
            tipus = u"Cessió&nbsp;d'ús"
        else:
            raise SuspiciousOperation()

        traspassos.append(dict(
            tipus=tipus,
            adreca=a,
            comentaris=a.traspas_comentaris_avaluacio or etiqueta_sense_comentaris
        ))

    concessions_activitat = []

    # concessions d'activitats associades a una adreça
    for adreca in proces.adreces.all():
        peticions = adreca.peticions_concessio_activitat.values_list('activitat', 'comentaris_avaluacio')
        peticions = {aid: com for (aid, com) in peticions}
        for activitat_id, activitat_nom in adreca.activitats.values_list('id', 'nom'):
            if activitat_id in peticions:
                concessions_activitat.append({
                    'adreca': adreca,
                    'activitat': activitat_nom,
                    'comentaris': peticions[activitat_id] or etiqueta_sense_comentaris
                })

    # concessions d'activitats no vinculades a cap adreça
    peticions = proces.peticions_concessio_activitat
    peticions = peticions.filter(adreca__isnull=True).values_list('activitat', 'comentaris_avaluacio')
    peticions = {aid: com for (aid, com) in peticions}
    for activitat in proces.altres_activitats.all():
        if activitat.id in peticions:
            concessions_activitat.append({
                'adreca': None,
                'activitat': activitat.nom,
                'comentaris': peticions[activitat.id] or etiqueta_sense_comentaris
            })

    # assegurances d'adreces i activitats vinculades a adreça
    assegurances_adreca = []
    for adreca in proces.adreces.all():
        peticio = proces.peticions_asseguranca.filter(adreca=adreca, activitat__isnull=True)
        if peticio.exists():
            assegurances_adreca.append({
                'adreca': adreca,
                'activitat': None,
                'comentaris': peticio[0].comentaris_avaluacio or etiqueta_sense_comentaris
            })

        for activitat in adreca.activitats.all():
            peticio = proces.peticions_asseguranca.filter(adreca=adreca, activitat=activitat)
            if peticio.exists():
                assegurances_adreca.append({
                    'adreca': adreca,
                    'activitat': activitat,
                    'comentaris': peticio[0].comentaris_avaluacio or etiqueta_sense_comentaris
                })

    # assegurances d'activitat no vinculades a cap adreça
    assegurances_altres_activitats = []
    for activitat in proces.altres_activitats.all():
        peticio = proces.peticions_asseguranca.filter(adreca__isnull=True, activitat=activitat)
        if peticio.exists():
            assegurances_altres_activitats.append({
                'adreca': None,
                'activitat': activitat,
                'comentaris': peticio[0].comentaris_avaluacio or etiqueta_sense_comentaris
            })

    # tots els elements que calen per emplenar la plantilla, recollits a "data"
    data = {
        'proces': proces,
        'traspassos': traspassos,
        'concessions': concessions_activitat,
        'assegurances_adreca': assegurances_adreca,
        'assegurances_altres_activitats': assegurances_altres_activitats,
        'NO_APLICA': etiqueta_sense_comentaris,
    }

    if format == 'html':
        return render(
            request,
            'alta_socies/autoocupat/document-llista_de_tasques.html',
            data
        )
    else:
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'filename="llistat-tasques.pdf"'
        generate_task_list_pdf(data, response)
        return response


@login_required
@user_passes_test(is_user_responsable_alta)
def sessions_del_mes(request, tipus, year=None, month=None):

    # TODO refactor treure els tipus de sessió d'aqui :)
    # TODO — keep in sync with alta_socies.forms.GeneradorSessions
    TIPUS_SESSIO_AVALUACIO = 'avaluacio'
    TIPUS_SESSIO_ALTA = 'alta'
    TIPUS_DE_SESSIONS = {
        TIPUS_SESSIO_AVALUACIO: u"Sessions d'avaluació",
        TIPUS_SESSIO_ALTA: u"Sessions d'alta",
    }

    cal = calendar.Calendar()

    if year and month:
        try:
            today = date(int(year), int(month), 1)
        except:
            raise Http404
    else:
        today = date.today()

    base = None

    if tipus == TIPUS_SESSIO_AVALUACIO:
        base = SessioAvaluacio
    elif tipus == TIPUS_SESSIO_ALTA:
        base = SessioAlta
    else:
        # TODO perquè haig d'explicitar que l'string és unicode??
        raise Http404(u"Tipus de sessió desconeguda: %s" % tipus)

    year = today.year
    month = today.month
    month_name = date(year, month, 1).strftime('%B')
    plain_weeks = cal.monthdatescalendar(year, month)

    weeks = []

    for w in plain_weeks:
        week = []
        for d in w:
            events = base.objects.filter(data__gte=d).filter(data__lt=d + timedelta(days=1))
            week.append(dict(date=d, events=events))
        weeks.append(week)

    return render(request, 'alta_socies/sessions_del_mes.html', {
        'tipus':      TIPUS_DE_SESSIONS[tipus],
        'year':       year,
        'month':      month,
        'month_name': month_name,
        'weeks':      weeks,
        'dow_names':  dow_names
    })


@login_required
@user_passes_test(is_user_responsable_alta)
def sessions_acollida_recents(request, des_de=7):

    fins = timezone.now()
    des_de = fins - timedelta(days=des_de)

    sessions = SessioAcollida.objects.filter(data__gt=des_de, data__lte=fins)

    return render(request, 'alta_socies/sessions_acollida_recents.html', {
        'sessions': sessions,
        'des_de':   des_de,
        'fins':     fins,
    })


@login_required
@user_passes_test(is_user_responsable_alta)
def llistat_proces_alta_projecte_autoocupat(request,
                                            estat=ProcesAltaAutoocupat.RESOLUCIO_EN_CURS,
                                            filtre_responsable=True):

    estats = {x: y for (x, y) in ProcesAltaAutoocupat.RESOLUCIONS}

    # if estat not in [x[0] for x in ProcesAltaAutoocupat.RESOLUCIONS]:
    if estat not in estats.keys():
        raise Http404()

    processos = ProcesAltaAutoocupat.objects.filter(resolucio__exact=estat)
    if filtre_responsable:
        processos = processos.filter(responsable=request.user)
    processos = processos.order_by('-updated_at')
    estat_display = estats[estat]

    return render(
        request,
        'alta_socies/autoocupat/llistat.html',
        {
            'processos':          processos,
            'estat':              estat,
            'estat_display':      estat_display,
            'filtre_responsable': filtre_responsable,
        }
    )


@login_required
@user_passes_test(is_user_responsable_alta)
def llistat_proces_alta_projecte_autoocupat_revisio_descripcions(request):

    processos = ProcesAltaAutoocupat.objects.filter(pas__gte=2)

    return render(
        request,
        'alta_socies/autoocupat/llistat_revisio_descripcions.html',
        {
            'processos': processos,
        }
    )


@login_required
@user_passes_test(is_user_responsable_alta)
def estadistiques_proces_alta_projecte_autoocupat(request):

    # fem un recompte dels processos d'alta "en curs" que hi ha a cada pas
    recomptes = ProcesAltaAutoocupat.objects.filter(resolucio=ProcesAltaAutoocupat.RESOLUCIO_EN_CURS)
    recomptes = recomptes.values('pas').annotate(recompte=Count('pas')).order_by('pas')
    recomptes = {pas: recompte for (pas, recompte) in recomptes.values_list('pas', 'recompte')}

    rebutjats = ProcesAltaAutoocupat.objects.filter(resolucio=ProcesAltaAutoocupat.RESOLUCIO_REBUTJAT).count()
    acceptats = ProcesAltaAutoocupat.objects.filter(resolucio=ProcesAltaAutoocupat.RESOLUCIO_ACCEPTAT).count()

    recompte_per_pas = []
    for p in range(1, max(ProcesAltaAutoocupat.PASSOS.keys()) + 1):
        r = None
        if p in recomptes:
            r = recomptes[p]
        recompte_per_pas.append({'pas': p, 'recompte': r})

    recompte_per_pas.append(dict(pas=u"Rebutjats", recompte=rebutjats))
    recompte_per_pas.append(dict(pas=u"Acceptats", recompte=acceptats))

    return render(
        request,
        'alta_socies/autoocupat/estadistiques.html',
        {
            'recompte_per_pas': recompte_per_pas,
        },
        context_instance=RequestContext(request))


# TODO Això no és cap "view"
def extreure_peticions_concessio_activitat(post):

    peticions = {}
    comentaris = {}
    for k, v in post.iteritems():
        if k.startswith('demana_concessio_') and v == u'on':
            key = k[17:]
            (adr_id, act_id) = map(int, key.split('_'))
            if adr_id == -1:
                adr_id = None
            peticions[key] = {
                'adreca_id': adr_id,
                'activitat_id': act_id,
            }
        elif k.startswith('comentaris_concessio_'):
            key = k[21:]
            comentaris[key] = v

    for k, v in comentaris.iteritems():
        if v and k in peticions:
            peticions[k]['comentaris'] = v

    return peticions.values()


# TODO Això no és cap "view"
def extreure_peticions_asseguranca(post):

    peticions = {}
    comentaris = {}
    for k, v in post.iteritems():
        if k.startswith('peticio_') and v == u'on':
            key = k[8:]
            (adr_id, act_id) = map(int, key.split('_'))
            if adr_id == -1:
                adr_id = None
            if act_id == -1:
                act_id = None
            peticions[key] = {
                'adreca_id': adr_id,
                'activitat_id': act_id,
            }
        elif k.startswith('comentaris_'):
            key = k[11:]
            comentaris[key] = v

    for k, v in comentaris.iteritems():
        if v and k in peticions:
            peticions[k]['comentaris'] = v

    return peticions.values()


@login_required
@user_passes_test(is_user_responsable_alta)
def proces_alta_projecte_autoocupat(request, id_proces=None, pas=None):

    user = request.user
    comanda = request.POST.get('anem_a') # retorna None si request.method=='GET' però en aquest cas no es fa servir
    extra_template_variables = {}

    if id_proces:

        proces = get_object_or_404(ProcesAltaAutoocupat, pk=id_proces)

        if user != proces.responsable:
            # TODO use the messaging framework (thanks @tati!) to inform of the redirection
            return redirect('alta_socies:resum_proces_alta_projecte_autoocupat', pk=proces.id)

        if not pas:
            pas = proces.pas
        else:
            # no deixem anar a passos posteriors, en cap cas! :)
            pas = int(pas)
            if pas > proces.pas:
                # TODO use the messaging framework (thanks @tati!) to inform of the redirection
                # TODO again, raise the issue of 'id' vs 'pk'
                return redirect('alta_socies:resum_proces_alta_projecte_autoocupat', pk=proces.id)

    else:
        proces = ProcesAltaAutoocupat()
        pas = proces.pas

    passos = ProcesAltaAutoocupat.PASSOS
    breadcrumbs = []
    for n, p in passos.iteritems():
        c = 'bc-futur'
        if n < proces.pas:
            c = 'bc-completat'
        elif n <= proces.pas_maxim_assolit:
            c = 'bc-reculat'

        if n == pas:
            m = u"\u25B2"
        else:
            m = ''

        breadcrumbs.append({
            'n': n,
            'nom': p['nom'],
            'css': c,
            'mark': m
        })

    destinacio_aqui = 'aqui'
    destinacio_seguent = 'seguent'
    destinacio_anterior = 'anterior'
    destinacio_nou = 'nou'

    def transicio():

        if comanda not in (destinacio_aqui, destinacio_seguent,
                           destinacio_anterior, destinacio_nou):
            raise SuspiciousOperation()

        # aquest procés ha avançat, encara que ens volguem quedar
        # al mateix pas, ir al anterior o marxar a un procés nou
        pas_destinacio = pas + 1

        proces.enregistra_canvi_i_salta_i_desa(user, pas, pas_destinacio)

        if comanda == destinacio_nou:
            return redirect('alta_socies:proces_alta_projecte_autoocupat')

        if comanda == destinacio_aqui:
            pas_destinacio -= 1
        elif comanda == destinacio_anterior:
            pas_destinacio -= 2

        return redirect(
            'alta_socies:proces_alta_projecte_autoocupat',
            id_proces=proces.id,
            pas=pas_destinacio
        )

    def vista_seleccio_persones(accio, rol_a_assignar, seleccio_inicial,
                                persones_a_excloure=[], camps_obligatoris=None,
                                escollir_minim=0, escollir_maxim=0):
        """
        Mecanisme per gestionar l'assignacio de persones a un rol; De moment serveix per sòcies afins, sòcia
        mentora, i aviat servirà també per membres de referència.t
        :param rol_a_assignar: text que ha de sortir al títol de la llista de persones seleccionades
        :param seleccio_inicial: dict {Persona.id: Persona. tots_els_detalls, ...} de persones inicialment incloses
        al rol; Nomès es té en compte aquest paràmetre quan la vista s'executa per atendre una petició HTTP GET
        :param persones_a_excloure: Array de Persona.id que no es poden seleccionar
        :param camps_obligatoris: llista de noms dels camps del formulari que han d'estar «informats» per poder
        afegir una persona a la llista.
        :param escollir_minim: mínim de persones que han d'estar assignades al rol
        :param escollir_maxim: màxim de persones que poden estar assignades al rol
        :return: dict amb els elements que calen per muntar el template, aka "extra_template_variables", o None si s'ha
        acabat el procés de selecció, i per tant el «caller» ha de fer "return transicio()" per anar a on calgui.
        """
        # TODO comprovar si es fa servir encara alguna cosa de l'outer scope que s'hauria de passar com a paràmetre!
        accio_modificar_persona = 'modificar_persona'
        accio_crear_persona = 'crear_persona'
        accio_cercar_persona = 'cercar_persona'
        error = None
        _persona = None

        assert(escollir_maxim >= escollir_minim)

        if request.method == 'POST':

            escollides = request.POST.get('persones_escollides')  # TODO aquest id hauria de venir del form
            if escollides:
                escollides = map(int, escollides.split(','))
            else:
                escollides = []

            if accio == accio_modificar_persona:
                _persona = get_object_or_404(Persona, id=request.POST.get('id_persona'))  # TODO aquest id hauria de venir del form

            elif accio == accio_crear_persona:
                _persona = Persona()

            if _persona:
                _form = FormulariCercaActualitzacioSeleccioPersones(data=request.POST, instance=_persona, camps_obligatoris=camps_obligatoris)
                if _form.is_valid():
                    _persona = _form.save()
                    # manera fàcil de buidar formulari; despres de crear o actualitzar s'afegeix i tornem a mode cerca
                    _form = FormulariCercaActualitzacioSeleccioPersones(camps_obligatoris=camps_obligatoris)
                    accio = accio_cercar_persona # això serveix per que es torni a mostrar el form de cercar

                else:
                    _persona = None

            else:

                if escollir_maxim and len(escollides) > escollir_maxim:
                    if escollir_maxim == 1:
                        error = u"No es pot escollir més d'una persona per aquest rol."
                    else:
                        error = u"No es poden escollir més de %d persones per aquest rol." % escollir_maxim

                elif len(escollides) < escollir_minim:
                    if escollir_minim == 1:
                        error = u"Com a mínim s'ha d'escollir una persona per aquest rol"
                    else:
                        error = u"Com a mínim s'han d'escollir %d persones per aquest rol" % escollir_minim

                if not error:

                    nova_llista = []
                    for _id_persona in escollides:
                        nova_llista.append(Persona.objects.get(pk=_id_persona))

                    return dict(seleccio_definitiva=nova_llista)

                else:

                    _form = FormulariCercaActualitzacioSeleccioPersones(data=request.POST, camps_obligatoris=camps_obligatoris)

            # si el POST no ens treu d'aqui (hi havia errors) hem de preservar la selecció
            # turns result into dict {id: value, id:value, ... }
            seleccio_inicial = {
                _id_persona: Persona.objects.get(id=_id_persona).tots_els_detalls
                for _id_persona in escollides}

            if _persona:
                assert(_persona.id not in escollides)
                seleccio_inicial[_persona.id] = _persona.tots_els_detalls

        else:

            _form = FormulariCercaActualitzacioSeleccioPersones(camps_obligatoris=camps_obligatoris)

        _extra_template_variables = {
            'f':                   _form,
            'persones_a_excloure': json.dumps(persones_a_excloure),
            'persones_inicials':   json.dumps(seleccio_inicial),  # TODO això hauria de passar dins de 'form'
            'MODIFICAR_PERSONA':   accio_modificar_persona,
            'CREAR_PERSONA':       accio_crear_persona,
            'comanda':             accio,
            'error':               error,
            'rol_a_assignar':      rol_a_assignar,
            'escollir_minim':      escollir_minim,
            'escollir_maxim':      escollir_maxim,
        }

        return dict(extra_template_variables=_extra_template_variables)

    def assignar_cooperativa():
        if proces.es_inclus_lleugerament_firaire():
            proces.cooperativa_assignada = Cooperativa.objects.get(pk=2)  # XIPU
        else:
            proces.cooperativa_assignada = Cooperativa.objects.get(pk=1)  # Interprofessionals

    if pas == 0:

        if request.method == 'POST':
            form = FormulariProjecte(data=request.POST, instance=proces)
            if form.is_valid():
                proces = form.save(commit=False)
                return transicio()
        else:
            form = FormulariProjecte(instance=proces)

        extra_template_variables = {
            'form': form,
        }

    elif pas == 1:

        membres_de_referencia = None
        if request.method == 'GET':
            membres_de_referencia = proces.membres_de_referencia.values_list('id', 'tots_els_detalls')
            # turns result into dict {id: value, id:value, ... }
            membres_de_referencia = {pk: text for (pk, text) in membres_de_referencia}

        resultat = vista_seleccio_persones(
            accio=comanda,
            rol_a_assignar=u"Membres de referència",
            seleccio_inicial=membres_de_referencia,
            camps_obligatoris=['nom', 'cognom1', 'telefon'])

        if 'seleccio_definitiva' in resultat:
            proces.membres_de_referencia = resultat['seleccio_definitiva']
            return transicio()
        else:
            assert('extra_template_variables' in resultat)
            extra_template_variables = resultat['extra_template_variables']

    elif pas == 2:

        if request.method == 'POST':

            form = FormulariDadesGeneralsProjecte(data=request.POST, instance=proces)
            if form.is_valid():
                proces = form.save(commit=False)
                assignar_cooperativa()

                return transicio()

        else:

            form = FormulariDadesGeneralsProjecte(instance=proces)

        extra_template_variables = {
            'form': form,
            'proces': proces
        }

    elif pas == 3:

        MembresFormSet = formset_factory(FormulariDetallsMembreDeReferencia, extra=0)

        if request.method == 'POST':

            formset = MembresFormSet(data=request.POST)
            if formset.is_valid():

                membres_queryset = proces.membres_de_referencia

                for form in formset:

                    try:
                        m = membres_queryset.get(pk=form.cleaned_data.get('id'))
                    except:
                        raise SuspiciousOperation

                    m.email = form.cleaned_data.get('email')

                    m.tipus_document_dni = form.cleaned_data.get('tipus_document_dni')
                    m.dni = form.cleaned_data.get('dni')
                    m.compte_integralces = form.cleaned_data.get('compte_integralces')

                    contribucions = []
                    fc = form.cleaned_data.get('contribucions')
                    for c in fc:
                        contribucions.append(get_object_or_404(ContribucioCIC, pk=int(c)))

                    habilitats = []
                    fh = form.cleaned_data.get('habilitats')
                    for h in fh:
                        habilitats.append(get_object_or_404(Habilitat, pk=int(h)))

                    hc = m.habilitats_contribucions
                    if not hc:
                        hc = HabilitatsContribucionsPersona(persona=m)
                        hc.save()

                    hc.habilitats = habilitats
                    hc.contribucions = contribucions
                    hc.detalls_habilitats = form.cleaned_data.get('detalls_habilitats')
                    hc.save()
                    m.save()

                return transicio()

        else:

            membres = proces.membres_de_referencia.all()
            initial = []
            for m in membres:

                detalls = {
                    'id': m.id,
                    'nom_sencer': m.nom_sencer,
                    'email': m.email,
                    'dni': m.dni,
                    'tipus_document_dni': m.tipus_document_dni,
                    'compte_integralces': m.compte_integralces,
                }

                hc = m.habilitats_contribucions
                if hc:
                    detalls['habilitats'] = [k[0] for k in hc.habilitats.all().values_list('id')]
                    detalls['contribucions'] = [k[0] for k in hc.contribucions.all().values_list('id')]
                    detalls['detalls_habilitats'] = hc.detalls_habilitats

                initial.append(detalls)

            formset = MembresFormSet(initial=initial)

        extra_template_variables = {
            'formset_membres_referencia': formset,
        }

    elif pas == 4:

        AdrecesFormSet = modelformset_factory(AdrecaProjecteAutoocupat,
                                              form=FormulariAdrecaProjecteAutoocupat,
                                              extra=1,
                                              min_num=1,
                                              can_delete=True)

        if request.method == 'POST':

            formset = AdrecesFormSet(data=request.POST)

            if formset.is_valid():

                formset.save()

                proces.adreces.add(*formset.new_objects)
                proces.adreces.remove(*formset.deleted_objects)

                return transicio()

        else:

            adreces = proces.adreces.all()
            formset = AdrecesFormSet(queryset=adreces)

        extra_template_variables = {
            'formset': formset,
        }

    elif pas == 5:

        if proces.tipus == ProcesAltaAutoocupat.TIPUS_AUTOOCUPAT_FIRAIRE:
            FormClass = FormulariAltresActivitatsProjecteAutoocupatFiraire
        else:
            FormClass = FormulariAltresActivitatsProjecteAutoocupatNoFiraire

        if request.method == 'POST':

            form = FormClass(data=request.POST, instance=proces)

            if form.is_valid():
                proces = form.save(commit=False)
                assignar_cooperativa()
                form.save_m2m()

                return transicio()

        else:

            form = FormClass(instance=proces)

        extra_template_variables = {
            'form': form,
        }

    elif pas == 6:

        activitats_per_adreca = []

        # activitats associades a una adreça
        for adreca in proces.adreces.all():
            peticions = {}
            if request.method == 'GET':
                peticions = adreca.peticions_concessio_activitat.values_list('activitat', 'comentaris_avaluacio')
                peticions = {aid: com for (aid, com) in peticions}
            for activitat_id, activitat_nom in adreca.activitats.values_list('id', 'nom'):
                fila = {
                    'adreca_id': adreca.id,
                    'adreca_designacio': adreca.designacio,
                    'activitat_id': activitat_id,
                    'activitat_nom': activitat_nom
                }
                if activitat_id in peticions:
                    fila['checked'] = True
                    fila['comentaris'] = peticions[activitat_id] or u""
                activitats_per_adreca.append(fila)

        # activitats sense adreça
        peticions = {}
        if request.method == 'GET':
            peticions = proces.peticions_concessio_activitat
            peticions = peticions.filter(adreca__isnull=True).values_list('activitat', 'comentaris_avaluacio')
            peticions = {aid: com for (aid, com) in peticions}
        for activitat in proces.altres_activitats.all():
            fila = {
                'adreca_id': -1,
                'adreca_designacio': u"(cap)",
                'activitat_id': activitat.id,
                'activitat_nom': activitat.nom
            }
            if activitat.id in peticions:
                fila['checked'] = True
                fila['comentaris'] = peticions[activitat.id] or u""
            activitats_per_adreca.append(fila)

        if request.method == 'POST':

            proces.peticions_concessio_activitat.all().delete()
            peticions = extreure_peticions_concessio_activitat(request.POST)
            for peticio in peticions:
                act = Activitat.objects.get(pk=peticio['activitat_id'])
                if peticio['adreca_id']:
                    adr = AdrecaProjecteAutoocupat.objects.get(pk=peticio['adreca_id'])
                else:
                    adr = None
                if 'comentaris' in peticio:
                    com = peticio['comentaris']
                else:
                    com = None
                PeticioConcessioActivitat(
                    proces_alta_autoocupat=proces,
                    activitat=act,
                    adreca=adr,
                    comentaris_avaluacio=com
                ).save()

            return transicio()

        extra_template_variables = {
            'activitats_per_adreca': activitats_per_adreca,
        }

    elif pas == 7:

        if request.method == 'POST':

            proces.peticions_asseguranca.all().delete()
            peticions = extreure_peticions_asseguranca(request.POST)
            for peticio in peticions:
                adreca = None
                if peticio['adreca_id']:
                    adreca = proces.adreces.get(pk=peticio['adreca_id'])
                activitat = None
                if peticio['activitat_id']:
                    activitat = Activitat.objects.get(pk=peticio['activitat_id'])
                com = None
                if 'comentaris' in peticio:
                    com = peticio['comentaris']

                PeticioAsseguranca(
                    proces_alta_autoocupat=proces,
                    activitat=activitat,
                    adreca=adreca,
                    comentaris_avaluacio=com
                ).save()

            return transicio()

        adreces = []
        for adreca in proces.adreces.all():
            adr_item = {'id': adreca.id, 'designacio': adreca.designacio}
            peticio = proces.peticions_asseguranca.filter(adreca=adreca, activitat__isnull=True)
            if peticio.exists():
                adr_item['peticio'] = True
                adr_item['comentaris'] = peticio[0].comentaris_avaluacio or u""

            activitats = []
            for activitat in adreca.activitats.all():
                act_item = {'id': activitat.id, 'nom': activitat.nom}
                peticio = proces.peticions_asseguranca.filter(adreca=adreca, activitat=activitat)
                if peticio.exists():
                    act_item['peticio'] = True
                    act_item['comentaris'] = peticio[0].comentaris_avaluacio or u""
                activitats.append(act_item)

            adr_item['activitats'] = activitats
            adreces.append(adr_item)

        altres_activitats = []
        for activitat in proces.altres_activitats.all():
            act_item = {'id': activitat.id, 'nom': activitat.nom, };
            peticio = proces.peticions_asseguranca.filter(adreca__isnull=True, activitat=activitat)
            if peticio.exists():
                act_item['peticio'] = True
                act_item['comentaris'] = peticio[0].comentaris_avaluacio or u""

            altres_activitats.append(act_item)

        extra_template_variables = {
            'adreces': adreces,
            'altres_activitats': altres_activitats,
        }

    elif pas == 8:

        if request.method == 'POST':

            form = FormulariFormaPagamentQuotesAutoocupat(data=request.POST, instance=proces)
            if form.is_valid():
                proces = form.save(commit=False)
                return transicio()

        else:

            form = FormulariFormaPagamentQuotesAutoocupat(instance=proces)

        extra_template_variables = {
            'form': form,
        }

    elif pas == 9:

        if request.method == 'POST':
            return transicio()

        membres = proces.membres_de_referencia.values('id', 'nom_sencer', 'email')

        extra_template_variables = {
            'membres': membres,
            'proces': proces,
        }

    elif pas == 10:

        membres = proces.membres_de_referencia.values('id', 'nom_sencer', 'email')

        if proces.sessio_moneda_realitzada:
            for m in membres:
                # aqui s'assumeix que no es modifica la llista de membres de referencia
                # despres d'haver passat aquest pas; Si fem 'readonly' els passos completats
                # no caldria aquesta feina
                m['checked'] = True

        if request.method == 'POST':

            form_projecte = FormulariAltaProjecteAutoocupatSessioMoneda(data=request.POST, instance=proces)
            if form_projecte.is_valid():
                c = 0
                for m in membres:
                    m_id = m['id']
                    if request.POST.get('assistent_%d' % m_id, None):
                        m['checked'] = True
                        c += 1

                proces.sessio_moneda_realitzada = (c == len(membres))

                if proces.sessio_moneda_realitzada:
                    return transicio()

        else:

            form_projecte = FormulariAltaProjecteAutoocupatSessioMoneda(instance=proces)

        extra_template_variables = {
            'form_projecte': form_projecte,
            'membres':       membres,
            'hi_son_tots':   proces.sessio_moneda_realitzada,
        }

    elif pas == 11:

        AdrecesFormSet = modelformset_factory(AdrecaProjecteAutoocupat,
                                              form=FormulariAltaProjecteAutoocupatCessioUs,
                                              extra=0)

        if request.method == 'POST':

            formset = AdrecesFormSet(data=request.POST)

            if formset.is_valid():
                formset.save()
                return transicio()

        else:

            adreces = proces.adreces.filter(traspas=AdrecaProjecteAutoocupat.TIPUS_TRASPAS_CESSIO)
            formset = AdrecesFormSet(queryset=adreces)

        extra_template_variables = {
            'formset': formset,
        }

    elif pas == 12:

        AdrecesFormSet = modelformset_factory(AdrecaProjecteAutoocupat,
                                              form=FormulariAltaProjecteAutoocupatLloguer,
                                              extra=0)

        if request.method == 'POST':

            formset = AdrecesFormSet(data=request.POST)

            if formset.is_valid():
                formset.save()
                return transicio()

        else:

            adreces = proces.adreces.filter(traspas=AdrecaProjecteAutoocupat.TIPUS_TRASPAS_LLOGUER)
            formset = AdrecesFormSet(queryset=adreces)

        extra_template_variables = {
            'formset': formset,
        }

    elif pas == 13:

        PeticionsFormSet = modelformset_factory(PeticioConcessioActivitat,
                                                form=FormulariAltaConcessioActivitat,
                                                extra=0)

        if request.method == 'POST':

            formset = PeticionsFormSet(data=request.POST)

            if formset.is_valid():
                formset.save()
                return transicio()

        else:

            peticions = proces.peticions_concessio_activitat.all().order_by('adreca', 'activitat')
            formset = PeticionsFormSet(queryset=peticions)

        extra_template_variables = {
            'formset': formset,
        }

    elif pas == 14:

        PeticionsFormSet = modelformset_factory(PeticioAsseguranca,
                                                form=FormulariAltaAsseguranca,
                                                extra=0)

        if request.method == 'POST':

            formset = PeticionsFormSet(data=request.POST)

            if formset.is_valid():
                formset.save()
                return transicio()

        else:

            peticions = proces.peticions_asseguranca.all().order_by('adreca', 'activitat')
            formset = PeticionsFormSet(queryset=peticions)

        extra_template_variables = {
            'formset': formset,
        }

    elif pas == 15:

        if request.method == 'POST':

            form = FormulariAltaQuotes(data=request.POST, instance=proces)

            if form.is_valid():
                proces = form.save(commit=False)
                proces.quota_alta_quantitat = proces.get_quota(proces.QUOTA_ALTA, proces.quota_alta_forma_pagament)
                proces.quota_avancada_quantitat = proces.get_quota(proces.QUOTA_TRIMESTRAL_AVANCADA, proces.quota_avancada_forma_pagament)
                proces.quota_alta_data_hora_pagament = proces.quota_avancada_data_hora_pagament = timezone.now()

                return transicio()

        else:

            form = FormulariAltaQuotes(instance=proces)

        algun_pagament_en_ecos = (proces.quota_alta_forma_pagament == proces.FORMA_PAGAMENT_ECO or
                                  proces.quota_avancada_forma_pagament == proces.FORMA_PAGAMENT_ECO)

        extra_template_variables = {
            'form': form,
            'algun_pagament_en_ecos': algun_pagament_en_ecos,
        }

    elif pas == 16:

        membres_de_referencia = proces.membres_de_referencia.values_list('id', flat=True)
        # turn django.db.models.query.ValuesListQuerySet into simple array of integers:
        membres_de_referencia = [x for x in membres_de_referencia]

        socies_afins_addicionals = None
        if request.method == 'GET':
            socies_afins_addicionals = proces.socies_afins_addicionals.values_list('id', 'tots_els_detalls')
            # turns result into dict {id: value, id:value, ... }
            socies_afins_addicionals = {pk: text for (pk, text) in socies_afins_addicionals}

        resultat = vista_seleccio_persones(
            accio=comanda,
            rol_a_assignar=u"Sòcies afins addicionals",
            seleccio_inicial=socies_afins_addicionals,
            persones_a_excloure=membres_de_referencia,
            camps_obligatoris=['nom', 'cognom1', 'dni'])

        if 'seleccio_definitiva' in resultat:
            proces.socies_afins_addicionals = resultat['seleccio_definitiva']
            return transicio()
        else:
            assert('extra_template_variables' in resultat)
            extra_template_variables = resultat['extra_template_variables']

    elif pas == 17:

        membres_de_referencia = proces.membres_de_referencia.values_list('id', flat=True)
        # turn django.db.models.query.ValuesListQuerySet into simple array of integers:
        membres_de_referencia = [x for x in membres_de_referencia]

        mentora = None
        if request.method == 'GET':
            mentora = proces.socia_mentora
            if mentora:
                mentora = {mentora.id: mentora.tots_els_detalls}
            else:
                mentora = {}

        resultat = vista_seleccio_persones(
            accio=comanda,
            rol_a_assignar=u"Sòcia mentora",
            seleccio_inicial=mentora,
            persones_a_excloure=membres_de_referencia,
            camps_obligatoris=['nom', 'cognom1', 'dni'],
            escollir_maxim=1,
            escollir_minim=1)

        if 'seleccio_definitiva' in resultat:
            proces.socia_mentora = resultat['seleccio_definitiva'][0]
            return transicio()
        else:
            assert('extra_template_variables' in resultat)
            extra_template_variables = resultat['extra_template_variables']

    elif pas == 18:

        error_falten_fotos = False

        if request.method == 'POST':

            volen_marxar = comanda != destinacio_aqui
            form = FormulariFotoProjecteAutoocupatFiraire(request.POST, request.FILES)
            formulari_valid = form.is_valid()
            formulari_buit = not (form.cleaned_data.get('nom') or form.cleaned_data.get('detalls') or request.FILES)

            firaire = proces.tipus == proces.TIPUS_AUTOOCUPAT_FIRAIRE

            if not formulari_buit and formulari_valid:

                foto = form.save(commit=False)
                foto.projecte = proces
                foto.save()

                filename = foto.imatge.file.name
                if create_thumbnail(filename):
                    elements = foto.imatge.url.split('/')
                    elements.insert(len(elements) - 1, settings.THUMBNAILS_SUBDIR)
                    foto.url_miniatura = '/'.join(elements)
                    foto.save()

            fotos = proces.fotos_parada_firaire.all()

            if formulari_buit or formulari_valid:
                eliminar = form.cleaned_data.get('imatges_a_eliminar')
                if eliminar:
                    for fid in eliminar.split(','):
                        proces.fotos_parada_firaire.get(pk=fid).delete();

                if not volen_marxar or (not firaire or len(fotos) >= 2):
                    return transicio()

            error_falten_fotos = firaire and volen_marxar and len(fotos) < 2

            if formulari_buit or formulari_valid:
                form = FormulariFotoProjecteAutoocupatFiraire()

        else:

            fotos = proces.fotos_parada_firaire.all()
            form = FormulariFotoProjecteAutoocupatFiraire()

        extra_template_variables = {
            'form': form,
            'fotos': fotos,
            'error_falten_fotos': error_falten_fotos,
            'amplada_marc': settings.THUMBNAIL_MAX_SIZE[0],
            'alcada_marc': settings.THUMBNAIL_MAX_SIZE[1],
        }

    elif pas == 19:

        if request.method == 'POST':

            form = ForulariPas19(data=request.POST, instance=proces)

            if form.is_valid():
                proces = form.save()
                return transicio()

        else:

            form = ForulariPas19(instance=proces)

        extra_template_variables = {
            'form': form,
        }

    elif pas == 20:

        if request.method == 'POST':

            proces.resolucio = 'acceptat'
            proces.data_resolucio = timezone.localtime(timezone.now())
            proces.save()
            projecte = ProjecteAutoocupat.objects.create_projecte(proces=proces)
            for peticio in proces.peticions_asseguranca.all():
                PolissaAsseguranca.objects.create_polissa(peticio=peticio, projecte=projecte)

            for foto in proces.fotos_parada_firaire.all():
                FotoProjecteAutoocupatFiraire.objects.create(projecte=projecte,
                                                             nom=foto.nom,
                                                             detalls=foto.detalls,
                                                             imatge=foto.imatge,
                                                             url_miniatura=foto.url_miniatura)
            messages.success(request, "El projecte s'ha creat correctament")
            return redirect('inici:index')

    # -----------------------------------

    template_variables = dict({

        'id_proces':     id_proces,  # TODO canviar a tots els templates per proces.id i eliminar aquesta variable
        'proces':        proces,
        'pas':           pas,
        'nom_pas':       passos[pas]['nom'],
        'breadcrumbs':   breadcrumbs,

        'ANEM_AQUI':     destinacio_aqui,
        'ANEM_SEGUENT':  destinacio_seguent,
        'ANEM_ANTERIOR': destinacio_anterior,
        'ANEM_NOU':      destinacio_nou,

    }.items() + extra_template_variables.items())

    return render(
        request,
        'alta_socies/autoocupat/%02d-%s.html' % (pas, passos[pas]['template']),
        template_variables
    )
