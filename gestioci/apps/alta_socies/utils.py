# coding=utf-8
from reportlab.lib.pagesizes import A4
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Table, TableStyle
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import cm, mm
from reportlab.lib import colors


def generate_task_list_pdf(data, response):
    """
    Genera un pdf con la lista de tareas referentes al proceso de alta
    """

    PAGE_WIDTH = 210*mm
    PAGE_HEIGHT = 297*mm

    DARK_GREEN = [0/255.0, 154/255.0, 49/255.0]
    LIGHT_GREEN = [132/255.0, 207/255.0, 150/255.0]
    WHITE_GREEN = [198/255.0, 231/255.0, 206/255.0]

    proces = data['proces']
    title = proces.nom
    description = "Llistat de tasques"

    styles = getSampleStyleSheet()
    styleH2 = styles['Heading2']
    styleD = styles['Definition']
    styleT = styles['Title']
    styleT.textColor = colors.Color(*DARK_GREEN)
    styleH2.textColor = colors.Color(*DARK_GREEN)

    TABLE_STYLE = TableStyle(
                  [('LINEABOVE', (0, 0), (-1, 0), 2, colors.green),
                   ('LINEABOVE', (0, 1), (-1, -1), 0.25, colors.black),
                   ('LINEBELOW', (0, -1), (-1, -1), 2, colors.green),
                   ('FONT', (0, 0), (-1, 0), 'Helvetica-Bold')])

    def first_page(canvas, doc):
        """
        Genera la portada del pdf
        """
        canvas.saveState()
        canvas.setFillColorRGB(*DARK_GREEN)
        canvas.rect(0, 0, PAGE_WIDTH, PAGE_HEIGHT, stroke=0, fill=1)
        canvas.setFillColorRGB(*WHITE_GREEN)
        canvas.setFont('Helvetica-Bold', 24)
        canvas.drawCentredString(PAGE_WIDTH/2.0, PAGE_HEIGHT/2.0, title)
        canvas.setFillColorRGB(*LIGHT_GREEN)
        canvas.setFont('Helvetica', 20)
        canvas.drawCentredString(PAGE_WIDTH/2.0, PAGE_HEIGHT/2.0 - 15*mm, "Cooperativa Integral Catalana")

    def later_page(canvas, doc):
        """
        Generar el template para las demás páginas
        """
        canvas.saveState()
        canvas.setFillColorRGB(*LIGHT_GREEN)
        canvas.setFont('Helvetica', 9)
        canvas.drawString(cm, 0.75*cm, "Pagina %d / %s / %s" % (doc.page, title, description))
        canvas.restoreState()

    doc = SimpleDocTemplate(response, pagesize=A4,
                            rightMargin=1.5*cm, leftMargin=1.5*cm,
                            topMargin=2.5*cm, bottomMargin=1.5*cm)
    # Skip first page
    Story = [Spacer(1, PAGE_HEIGHT-5*cm)]
    # Common title
    title_paragraph = Paragraph(u"Llistat de Tasques", styleT)
    Story.append(title_paragraph)
    # Introduction
    sub1_p = Paragraph(u"Informació", styleH2)
    Story.append(sub1_p)
    intro = u"A continuació, us indiquem la llista de tasques que heu de realitzar. \
            Per qualsevol dubte disposeu de la pàgina web de la CIC. En cas necessari, també \
            podeu acudir al responsable amb qui heu fet la sessió d'Avaluació. Quan tingueu \
            totes aquestes tasques completades haureu de demanar hora amb aquest responsable \
            per venir un altre dia — les mateixes persones — a completar el procés d'alta del \
            vostre projecte. IMPORTANT: si no aporteu tota la documentació i tasques demanades \
            en aquesta llista no podrem completar el procés encara que vingueu a una sessió \
            d'Alta, i l'haureu de repetir un altre dia."
    intro_p = Paragraph(intro, styleD)
    Story.append(intro_p)
    # Responsable d'alta
    sub2_p = Paragraph(u"Responsable d'alta", styleH2)
    Story.append(sub2_p)
    responsable = "Nom: %s %s" % (data['proces'].responsable.first_name, data['proces'].responsable.last_name)
    email = u"Adreça de contacte: %s" % data['proces'].responsable.email
    resp_p = Paragraph(responsable, styleD)
    Story.append(resp_p)
    email_p = Paragraph(email, styleD)
    Story.append(email_p)
    # Traspassos
    traspassos = data['traspassos']
    if traspassos:
        sub3_p = Paragraph("Traspassos", styleH2)
        Story.append(sub3_p)
        table_data = [["Tipus", u"Adreça", "Comentaris"]]
        for traspas in traspassos:
            tipus = traspas['tipus']
            adreca = ""
            if traspas['adreca']:
                adreca += "%s, %s" % (traspas['adreca'].adreca, traspas['adreca'].poblacio)
            else:
                adreca += "No aplica"
            comentaris = traspas['comentaris'] or "No comments"
            table_data.append([tipus, adreca, comentaris])
        table = Table(table_data, style=TABLE_STYLE)
        Story.append(table)
        p = Paragraph(u"Haureu d'aportar el document corresponent a cada traspàs.", styleD)
        Story.append(p)
    # Concessions
    concessions = data['concessions']
    if concessions:
        sub4_p = Paragraph("Concessions", styleH2)
        Story.append(sub4_p)
        table_data = [["Activitat", u"Adreça", "Comentaris"]]
        for concessio in concessions:
            activitat = concessio['activitat']
            adreca = ""
            if concessio['adreca']:
                adreca += "%s, %s" % (concessio['adreca'].adreca, concessio['adreca'].poblacio)
            else:
                adreca += "No aplica"
            comentaris = concessio['comentaris'] or "No commments"
            table_data.append([activitat, adreca, comentaris])
        table = Table(table_data, style=TABLE_STYLE)
        Story.append(table)
        p = Paragraph(u"Haureu d'aportar la llicència d'activitat i el contracte de concessió corresponents a cada activitat.", styleD)
        Story.append(p)
    # Assegurançes
    assegurances_amb_adreca = data['assegurances_adreca']
    assegurances_activitat = data['assegurances_altres_activitats']
    if assegurances_amb_adreca or assegurances_activitat:
        sub5_p = Paragraph(u"Assegurançes", styleH2)
        Story.append(sub5_p)
        table_data = [["Activitat", u"Adreça", "Comentaris"]]
        for ass in assegurances_amb_adreca:
            activitat = ass['activitat'].nom
            adreca = ""
            if ass['adreca']:
                adreca += "%s, %s" % (ass['adreca'].adreca, ass['adreca'].poblacio)
            else:
                adreca += "No aplica"
            comentaris = ass['comentaris'] or "No commments"
            table_data.append([activitat, adreca, comentaris])
        for ass in assegurances_activitat:
            activitat = ass['activitat'].nom
            adreca = ""
            if ass['adreca']:
                adreca += "%s, %s" % (ass['adreca'].adreca, ass['adreca'].poblacio)
            else:
                adreca += "No aplica"
            comentaris = ass['comentaris'] or "No commments"
            table_data.append([activitat, adreca, comentaris])
        table = Table(table_data, style=TABLE_STYLE)
        Story.append(table)
        p = Paragraph(u"Haureu d'aportar el rebut vigent i el certificat de cobertura corresponents a cada assegurança.", styleD)
        Story.append(p)
    # Firaire
    if proces.tipus == proces.TIPUS_AUTOOCUPAT_FIRAIRE:
        sub6_p = Paragraph("Firaire", styleH2)
        Story.append(sub6_p)
        list_1_p = Paragraph("* Foto de la parada", styleD)
        list_2_p = Paragraph("* Fotos dels productes i/o gammes de productes", styleD)
        list_3_p = Paragraph(u"* Descripció detallada de la parada i els productes", styleD)
        Story.extend([list_1_p, list_2_p, list_3_p])
        p = Paragraph("Porteu les fotos i descripcions en USB, o adjunteu-les a un correu electrònic al vostre responsable.", styleD)
        Story.append(p)
    # Quotes
    sub7_p = Paragraph("Quotes", styleH2)
    Story.append(sub7_p)
    quota_alta = "* Quota d'alta: %d %s" % (proces.quota_alta_quantitat, proces.get_quota_alta_forma_pagament_display())
    quota_avancada = u"* Quota trimestral avançada: %d %s" % (proces.quota_avancada_quantitat, proces.get_quota_avancada_forma_pagament_display())
    list_1_p = Paragraph(quota_alta, styleD)
    list_2_p = Paragraph(quota_avancada, styleD)
    Story.extend([list_1_p, list_2_p])
    p = Paragraph(u"Si les quotes es paguen en moneda social caldrà portar el número d'un compte CES que ja tingueu.", styleD)
    Story.append(p)
    # Socis afins
    if proces.individual == proces.INDIVIDUAL_COL_LECTIU:
        sub8_p = Paragraph(u"Socis afins", styleH2)
        Story.append(sub8_p)
        list_1_p = Paragraph("* Portar apuntats els nom, cognom i DNI de tots socis afins del projecte.", styleD)
        Story.append(list_1_p)
        p = Paragraph(u"Són socis afins els membres de referència, i també els membres que vulguin rebre cobertura legal de la CIC.", styleD)
        Story.append(p)

    doc.build(Story, onFirstPage=first_page, onLaterPages=later_page)
