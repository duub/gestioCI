# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('alta_socies', '0007_procesaltaautoocupat_iva_assignat'),
    ]

    operations = [
        migrations.CreateModel(
            name='FotoProjecteAutoocupatFiraire',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nom', models.CharField(max_length=256)),
                ('detalls', models.TextField(null=True, blank=True)),
                ('imatge', models.ImageField(upload_to=b'fotos_projecte_autoocupat_firaire')),
                ('projecte', models.ForeignKey(related_name='fotos_parada_firaire', to='alta_socies.ProcesAltaAutoocupat')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
