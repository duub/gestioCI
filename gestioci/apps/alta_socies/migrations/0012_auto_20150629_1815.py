# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('alta_socies', '0011_crear_grup_exportadores'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='sessio',
            options={'ordering': ['-data']},
        ),
        migrations.AlterField(
            model_name='peticioconcessioactivitat',
            name='data_final_concessio',
            field=models.DateField(help_text="Data d'acabament del contracte que signa el projecte amb la CIC", null=True, verbose_name='data final de la concessi\xf3 a la CIC', blank=True),
        ),
        migrations.AlterField(
            model_name='peticioconcessioactivitat',
            name='data_final_llicencia',
            field=models.DateField(help_text="Data d'acabament del contracte amb l'Ajuntament", null=True, verbose_name='data final del document original', blank=True),
        ),
        migrations.AlterField(
            model_name='peticioconcessioactivitat',
            name='data_inici_concessio',
            field=models.DateField(help_text="Data d'inici del contracte que signa el projecte amb la CIC", null=True, verbose_name="data d'inici de la concessi\xf3 a la CIC", blank=True),
        ),
        migrations.AlterField(
            model_name='peticioconcessioactivitat',
            name='data_inici_llicencia',
            field=models.DateField(help_text="Data d'inici del contracte amb l'Ajuntament", null=True, verbose_name="data d'inici del document original", blank=True),
        ),
        migrations.AlterField(
            model_name='procesaltaautoocupat',
            name='adreces',
            field=models.ManyToManyField(related_name='xxx_proces_alta_autoocupat', to='socies.AdrecaProjecteAutoocupat', blank=True),
        ),
        migrations.AlterField(
            model_name='procesaltaautoocupat',
            name='altres_activitats',
            field=models.ManyToManyField(help_text='Altres activitats no vinculades a cap adre\xe7a', to='socies.Activitat', blank=True),
        ),
        migrations.AlterField(
            model_name='procesaltaautoocupat',
            name='email',
            field=models.EmailField(max_length=254),
        ),
        migrations.AlterField(
            model_name='procesaltaautoocupat',
            name='membres_de_referencia',
            field=models.ManyToManyField(related_name='altes_projecte_autoocupat', verbose_name='membres de refer\xe8ncia', to='socies.Persona', blank=True),
        ),
        migrations.AlterField(
            model_name='procesaltaautoocupat',
            name='socies_afins_addicionals',
            field=models.ManyToManyField(related_name='+', to='socies.Persona', blank=True),
        ),
        migrations.AlterField(
            model_name='sessio',
            name='duracio',
            field=models.PositiveSmallIntegerField(help_text='En minuts', verbose_name='Duraci\xf3'),
        ),
    ]
