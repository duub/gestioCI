# -*- coding: utf-8 -*-

from django.db import migrations

from gestioci.settings import auth_groups


def crear_grup_exportadores(apps, schema_editor):
    """
    Aquest mètode serveix com a plantilla per crear grups al futur;
    """
    Group = apps.get_model('auth', 'Group')

    verbose = False
    noms_dels_grups = [auth_groups.EXPORTADORES, ]

    if verbose:
        print()

    for nom in noms_dels_grups:
        try:
            u = Group.objects.get(name=nom)
            if verbose:
                print '''    = "%s": el grup ja existeix''' % nom
        except Group.DoesNotExist:
            Group.objects.create(name=nom)
            if verbose:
                print '''    + "%s": el grup no existia però s'ha creat''' % nom


class Migration(migrations.Migration):

    dependencies = [
        ('alta_socies', '0010_auto_20150217_2237'),
    ]

    operations = [
        migrations.RunPython(crear_grup_exportadores),
    ]
