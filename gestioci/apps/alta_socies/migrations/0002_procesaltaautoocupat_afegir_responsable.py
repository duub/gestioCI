# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


def crear_usuari_nobody(apps, schema_editor):
    User = apps.get_model('auth', 'User')
    try:
        u = User.objects.get(username='nobody')
        if u.is_active:
            u.is_active = False
            u.save()
    except User.DoesNotExist:
        User(username='nobody', is_active=False).save()


def omplir_responsables_buits(apps, schema_editor):
    ProcesAltaAutoocupat = apps.get_model('alta_socies', 'ProcesAltaAutoocupat')
    User = apps.get_model('auth', 'User')
    nobody = User.objects.get(username='nobody')
    for p in ProcesAltaAutoocupat.objects.filter(responsable__isnull=True):
        p.responsable = nobody
        p.save()


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('alta_socies', '0001_initial'),
    ]

    operations = [

        # afegir un usuari (User) inactiu amb username='nobody'
        migrations.RunPython(crear_usuari_nobody),

        # afegim el camp 'responsable' que no és obligatori
        migrations.AddField(
            model_name='procesaltaautoocupat',
            name='responsable',
            field=models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, help_text="\xdaltim responsable d'alta que ha modificat el proc\xe9s", null=True),
            preserve_default=True,
        ),

        # emplenem els NULLs del camp responsable amb l'usuari nobody
        migrations.RunPython(omplir_responsables_buits),

        # fem obligatori (not null) el camp responsable
        migrations.AlterField(
            model_name='procesaltaautoocupat',
            name='responsable',
            field=models.ForeignKey(help_text="\xdaltim responsable d'alta que ha modificat el proc\xe9s", to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),

    ]
