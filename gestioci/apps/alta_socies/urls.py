from django.conf.urls import patterns, include, url

from .views import SessioMonedaListView, SessioMonedaCreateView
from .views import SessioAcollidaListView, SessioAcollidaCreateView, \
    SessioAcollidaUpdateView, SessioMonedaUpdateView
from .views import ProcesAltaAutoocupatDetailView

urlpatterns = patterns(

    'gestioci.apps.alta_socies.views',

    # -----------------
    # SOCIS AUTOOCUPATS
    # -----------------

    url(r'^rebutjar_proces_alta/(?P<proces_id>\d+)/$', 'rebutjar_proces_alta_autoocupat', name='rebutjar_proces'),

    url(r'^exportar_a_csv/$', 'exportar_a_csv', name='exportar_a_csv'),

    url(r'^autoocupat/resum/(?P<pk>\d+)/$', ProcesAltaAutoocupatDetailView.as_view(), name='resum_proces_alta_projecte_autoocupat'),

    url(r'^editar_sessio_moneda/(?P<pk>\d+)/$', SessioMonedaUpdateView.as_view(), name='editar_sessio_moneda'),

    url(r'^editar_sessio_acollida/(?P<pk>\d+)/$', SessioAcollidaUpdateView.as_view(), name='editar_sessio_acollida'),

    url(r'^llistat_sessio_acollida', SessioAcollidaListView.as_view(), name='llistat_sessio_acollida'),

    url(r'^crear_sessio_acollida', SessioAcollidaCreateView.as_view(), name='crear_sessio_acollida'),

    url(r'^llistat_sessio_moneda', SessioMonedaListView.as_view(), name='llistat_sessio_moneda'),

    url(r'^crear_sessio_moneda', SessioMonedaCreateView.as_view(), name='crear_sessio_moneda'),

    url(r'^generador_sessions/$',
        'generar_sessions',
        name='generador_sessions'),

    url(r'^sessions_del_mes/(?P<tipus>\w+)(/(?P<year>\d\d\d\d)/(?P<month>\d\d?))?$',
        'sessions_del_mes',
        name='sessions_del_mes'),

    url(r'^autoocupat/$',
        'proces_alta_projecte_autoocupat',
        name='proces_alta_projecte_autoocupat'),

    url(r'^autoocupat/(?P<id_proces>\d+)/$',
        'proces_alta_projecte_autoocupat',
        name='proces_alta_projecte_autoocupat'),

    url(r'^autoocupat/(?P<id_proces>\d+)/(?P<pas>\d+)/$',
        'proces_alta_projecte_autoocupat',
        name='proces_alta_projecte_autoocupat'),

    url(r'^autoocupat/llista-tasques/(?P<id_proces>\d+)/$',
        'proces_alta_projecte_autoocupat_llista_de_tasques',
        name='proces_alta_projecte_autoocupat-llista_de_tasques'),

    url(r'^autoocupat/llista-tasques/(?P<id_proces>\d+)/(?P<format>pdf|html)/$',
        'proces_alta_projecte_autoocupat_llista_de_tasques',
        name='proces_alta_projecte_autoocupat-llista_de_tasques'),

    url(r'^autoocupat/processos/(?P<estat>\w+)/$',
        'llistat_proces_alta_projecte_autoocupat',
        name='llistat_proces_alta_projecte_autoocupat'),

    url(r'^autoocupat/processos/(?P<estat>\w+)/tots$',
        'llistat_proces_alta_projecte_autoocupat', {'filtre_responsable': False},
        name='llistat_proces_alta_projecte_autoocupat-tots'),

    url(r'^autoocupat/control-qualitat/$',
        'llistat_proces_alta_projecte_autoocupat_revisio_descripcions',
        name='llistat_proces_alta_projecte_autoocupat_revisio_descripcions'),

    url(r'^estadistiques_proces_alta_projecte_autoocupat/?$',
        'estadistiques_proces_alta_projecte_autoocupat',
        name='estadistiques_proces_alta_projecte_autoocupat'),

    # -----------------
    # SOCIS COOPERATIUS
    # -----------------
    url(r'^cooperatiu/$', 'cooperatiu', name='cooperatiu'),
)
