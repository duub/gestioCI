# coding=utf-8

from django import forms
from django.core.exceptions import ValidationError

from gestioci.apps.facturacio.models import FacturaEmesa, Trimestre


class FormCrearFacturaEmesa(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        clients = kwargs.pop('clients')
        super(FormCrearFacturaEmesa, self).__init__(*args, **kwargs)
        self.fields['client'].queryset = clients

    class Meta:
        model = FacturaEmesa
        fields = ['client', 'data']

    def clean_data(self):

        data = self.cleaned_data['data']

        if not Trimestre.objects.filter(obert=True, data_inici__lte=data, data_final__gte=data).exists():
            raise forms.ValidationError(u"La data ha d'estar dintre d'un dels trimestres oberts")

        return data
