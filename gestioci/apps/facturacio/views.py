# coding=utf-8
from django.contrib.auth.decorators import login_required
from django.http import Http404
from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone
from django.views.generic import View

from gestioci.apps.projectes.models import ProjecteAutoocupat
from gestioci.apps.empreses.models import Empresa
from gestioci.apps.facturacio.forms import FormCrearFacturaEmesa
from gestioci.apps.socies.models import PersonaUsuaria
from .models import QuotaTrimestral, FacturaEmesa, FacturaRebuda, Trimestre


def llistat_quotes_trimestrals(request):
    today = timezone.localtime(timezone.now()).date()

    dates_vigor = QuotaTrimestral.objects.order_by('-data_vigor').values_list('data_vigor').distinct()
    dates_vigor = [d[0] for d in dates_vigor]

    taules = []
    hem_passat_actual = 0

    for data_vigor in dates_vigor:

        taula = QuotaTrimestral.objects.filter(data_vigor=data_vigor).order_by('base_imposable_minima')

        if data_vigor > today:
            temps = 'futur'
        else:

            if data_vigor <= today and hem_passat_actual == 0:
                temps = 'present'
            else:
                temps = 'passat'

            hem_passat_actual += 1

        taules.append(dict(data_vigor=data_vigor,
                           taula=taula,
                           temps=temps))

        if hem_passat_actual > 1:
            break

    return render(
        request,
        'facturacio/llistat_quotes_trimestrals.html',
        dict(taules=taules)
    )


@login_required
def editar_factura(request, id_factura):

    try:
        f = FacturaEmesa.objects.get(pk=id_factura)
        return redirect('facturacio:editar_factura_emesa', id_factura=id_factura)
    except FacturaEmesa.DoesNotExist:
        pass

    try:
        f = FacturaRebuda.objects.get(pk=id_factura)
        return redirect('facturacio:editar_factura_rebuda', id_factura=id_factura)
    except FacturaRebuda.DoesNotExist:
        pass

    raise Http404('Aquesta factura no existeix')


# això no és cap vista!!
# TODO --- pendent de decidir l'arquitectura de models que ens convé
def _projectes(user):

    try:
        persona = PersonaUsuaria.objects.get(usuari=user).persona
        resultat = persona.projectes_autoocupats.all()
    except PersonaUsuaria.DoesNotExist:
        resultat = ProjecteAutoocupat.objects.none()

    return resultat


@login_required
def crear_factura_emesa(request, id_client=None):

    projectes = _projectes(request.user)
    if not projectes:
        raise ValueError(u"oops! no estas donat d'alta com a membre de referència de cap projecte autoocupat.")

    if projectes.count() > 1:
        raise ValueError(u"oops! de moment no està previst que una persona pugui facturar a dos projectes.")
    else:
        projecte = projectes[0]

    clients_habituals = projecte.clients()
    initial = {}
    if id_client:
        client_proposat = Empresa.objects.filter(pk=id_client)  # vull una queryset pq despres ens convé la "unió"
        if client_proposat.count():
            initial['client'] = client_proposat[0].id
            clients_habituals |= client_proposat

    form = None

    if clients_habituals.count():

        if request.method == 'POST':

            Trimestre.crear_trimestres_necessaris()

            form = FormCrearFacturaEmesa(clients=clients_habituals, initial=initial, data=request.POST)

            if form.is_valid():

                factura = form.save(commit=False)
                factura.projecte = projecte
                factura.proveidor = projecte.cooperativa_assignada
                factura.save()

                return redirect('facturacio:editar_factura_emesa', id_factura=factura.pk)

        else:

            form = FormCrearFacturaEmesa(clients=clients_habituals, initial=initial)

    return render(
        request,
        'facturacio/crear_factura_emesa.html',
        dict(
            form=form,
        )
    )


@login_required
def editar_factura_emesa(request, id_factura):

    factura = get_object_or_404(FacturaEmesa, pk=id_factura)
    # TODO comprovar que l'usuari pertany al "ProjecteAutoocupat" ("SociaAmbFacturacioCooperativa")!
    # TODO --- pendent de decidir l'arquitectura de models que ens convé
    # TODO comprovar que la factura correspon al trimestre en curs, i que el trimestre no està tancat!

    return render(
        request,
        'facturacio/editar_factura_emesa.html',
        dict(
            factura=factura
        )
    )


@login_required
def crear_factura_rebuda(request, id_proveidor=None):
    pass


@login_required
def editar_factura_rebuda(request, id_factura):
    pass