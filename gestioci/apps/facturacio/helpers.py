# coding=utf-8
from datetime import date
from calendar import monthrange


def calcular_trimestre(data):
    """
    Retorna (any, trimestre, inici, final) per al trimestre que inclou la data especificada.
    Trimestre és un enter d'aquests: [1, 2, 3, 4]
    """
    year = data.year
    trimestre = [1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4][data.month-1]
    primer_mes = (trimestre - 1) * 3 + 1
    ultim_mes = primer_mes + 2
    inici = date(year, primer_mes, 1)
    final = date(year, ultim_mes, monthrange(year, ultim_mes)[1])
    return year, trimestre, inici, final
