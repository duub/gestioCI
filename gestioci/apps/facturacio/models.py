# coding=utf-8
from datetime import timedelta, date

from django.db import models
from django.db.models import Max
from django.utils import timezone

from gestioci.apps.projectes.models import ProjecteAutoocupat
from gestioci.apps.empreses.models import Empresa
from gestioci.apps.facturacio.helpers import calcular_trimestre


class QuotaTrimestral(models.Model):

    base_imposable_minima = models.DecimalField(max_digits=8, decimal_places=2)
    import_quota_trimestral = models.DecimalField(max_digits=8, decimal_places=2)
    data_vigor = models.DateField(verbose_name=u"data d'entrada en vigor")


# TODO això hauria d'anar a "Soci" (quan es defineixi aquesta abstracció)
# o si nomes aplica a projectes autoocupats s'haurien de canviar els noms de
# QuotaTrimestral i d'aquesta funció per fer-ho palès
def trobar_quota_trimestral(data, base_imposable):

    # la taula de quotes que aplica és la que té la data d'entrada en vigor màxima tal que
    # la data d'entrada en vigor és menor o igual a la data en que es vol fer el moviment
    data_vigor = QuotaTrimestral.objects.filter(data_vigor__lte=data).aggregate(Max('data_vigor'))['data_vigor__max']

    if data_vigor is None:
        raise ValueError(u"No hi ha quotes trimestrals definides per aquesta data (%s)" % data.isoformat())

    # donada aquesta data d'entrada en vigor, trobem el tram de facturació i la quota trimestral que aplica:
    # de tots els trams per imports inferiors o iguals a la facturació que s'indica, ens quedem amb el que
    # correspon a l'import més gran
    trobar_quota = QuotaTrimestral.objects.filter(data_vigor=data_vigor)
    trobar_quota = trobar_quota.filter(base_imposable_minima__lte=base_imposable)
    trobar_quota = trobar_quota.order_by('-base_imposable_minima')

    trobar_quota = list(trobar_quota)

    if len(trobar_quota):
        return trobar_quota[0].import_quota_trimestral

    raise ValueError(u"Manca un tram amb 'base imposable minima' = 0 a 'quota trimestral' "
                     u"per 'data d'entrada en vigor' = %s" % data_vigor)


class Factura(models.Model):

    FORMA_PAGAMENT_EFECTIU = 'efectiu'
    FORMA_PAGAMENT_TRANSFERENCIA = 'transferencia'

    FORMES_DE_PAGAMENT = (
        (FORMA_PAGAMENT_EFECTIU, u"En efectiu"),
        (FORMA_PAGAMENT_TRANSFERENCIA, u"Gestiona la cooperativa"),
    )

    projecte = models.ForeignKey(ProjecteAutoocupat)

    proveidor = models.ForeignKey(Empresa, related_name='factures_emeses')
    client = models.ForeignKey(Empresa, related_name='factures_rebudes')

    data = models.DateField()
    year = models.SmallIntegerField()
    numero = models.CharField(max_length=64, blank=False, null=False)

    import_total = models.DecimalField(default=0, max_digits=8, decimal_places=2)
    iva_total = models.DecimalField(default=0, max_digits=8, decimal_places=2)

    data_venciment = models.DateField(null=True, blank=True)
    data_prevista_pagament = models.DateField(null=True, blank=True)
    forma_de_pagament = models.CharField(default=FORMA_PAGAMENT_EFECTIU, max_length=16, choices=FORMES_DE_PAGAMENT)

    class Meta:
        verbose_name_plural = u"factures"


class FacturaEmesa(Factura):

    ESTAT_EMESA = 'emesa'
    ESTAT_COBRADA = 'cobrada'

    ESTATS = (
        (ESTAT_EMESA, u"Pendent de cobrar"),
        (ESTAT_COBRADA, u"Cobrada"),
    )

    estat = models.CharField(max_length=16, choices=ESTATS, default=ESTAT_EMESA)
    recarreg_equivalencia = models.DecimalField(default=0, max_digits=8, decimal_places=2,
                                                verbose_name=u"recàrreg d'equivalència")

    class Meta:
        verbose_name_plural = u"factures emeses"


class FacturaRebuda(Factura):

    ESTAT_REBUDA = 'rebuda'        # s'ha creat la factura al portal del soci
    ESTAT_ACCEPTADA = 'acceptada'  # s'ha rebut la factura a GE (en paper?) i s'ha acceptat que es pot comptabilitzar
    ESTAT_PAGADA = 'pagada'        # s'ha pagat TODO: cal això, o amb "acceptada" ja es te en compte al balanç i prou?

    ESTATS = (
        (ESTAT_REBUDA, u"Pendent de revisar"),
        (ESTAT_ACCEPTADA, u"Pendent de pagar"),
        (ESTAT_PAGADA, u"Pagada"),
    )

    estat = models.CharField(max_length=16, choices=ESTATS, default=ESTAT_REBUDA)
    retencio_irpf = models.DecimalField(default=0, max_digits=8, decimal_places=2,
                                        verbose_name=u"retenció IRPF")

    class Meta:
        verbose_name_plural = u"factures rebudes"


class LiniaFactura(models.Model):

    factura = models.ForeignKey(Factura)

    quantitat = models.DecimalField(max_digits=8, decimal_places=2)
    concepte = models.CharField(max_length=128)
    preu_unitari = models.DecimalField(max_digits=8, decimal_places=2)
    tipus_iva = models.SmallIntegerField()

    class Meta:
        verbose_name = u"línia de factura"
        verbose_name_plural = u"línies de factura"

class Trimestre(models.Model):

    nom = models.CharField(max_length=10, unique=True)

    data_inici = models.DateField()
    data_final = models.DateField()
    data_limit_tancament = models.DateField()

    obert = models.BooleanField(default=False)

    @property
    def tancat(self):
        return not self.obert

    @staticmethod
    def crear_trimestre(data, obert=False):
        """
        Crea i retorna el Trimestre que hi ha "al voltant" (que inclou) la data que s'especifica.
        """
        year, trimestre, inici, final = calcular_trimestre(data)
        limit_tancament = final + timedelta(days=10)
        return Trimestre.objects.create(
            nom='%dT%d' % (year, trimestre),
            data_inici=inici,
            data_final=final,
            data_limit_tancament=limit_tancament,
            obert=obert)

    @staticmethod
    def crear_trimestres_necessaris():
        avui = timezone.localtime(timezone.now()).date()
        t_actual = Trimestre.objects.filter(data_inici__lte=avui, data_final__gte=avui)
        assert t_actual.count() < 2
        if t_actual.count() == 0:
            t_actual = Trimestre.crear_trimestre(data=avui, obert=True)
        else:
            t_actual = t_actual[0]

        # la data límit de tancament d'un trimestre sempre cau dintre del següent trimestre
        d_seguent = t_actual.data_limit_tancament
        t_seguent = Trimestre.objects.filter(data_inici__lte=d_seguent, data_final__gte=d_seguent)
        assert t_seguent.count() < 2
        if t_seguent.count() == 0:
            t_seguent = Trimestre.crear_trimestre(data=d_seguent, obert=True)

    def __unicode__(self):
        return self.nom
