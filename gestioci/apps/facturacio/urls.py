from django.conf.urls import patterns, include, url

urlpatterns = patterns(

    'gestioci.apps.facturacio.views',

    url(r'^llistat_quotes_trimestrals/$', 'llistat_quotes_trimestrals', name='llistat_quotes_trimestrals'),

    url(r'^editar_factura/(?P<id_factura>\d+)$',        'editar_factura',        name='editar_factura'),

    url(r'^crear_factura_emesa/$',                      'crear_factura_emesa',   name='crear_factura_emesa'),
    url(r'^crear_factura_emesa/(?P<id_client>\d+)$',    'crear_factura_emesa',   name='crear_factura_emesa'),
    url(r'^editar_factura_emesa/(?P<id_factura>\d+)$',  'editar_factura_emesa',  name='editar_factura_emesa'),

    url(r'^crear_factura_rebuda/$',                     'crear_factura_rebuda',  name='crear_factura_rebuda'),
    url(r'^crear_factura_rebuda/(?P<id_factura>\d+)$',  'crear_factura_rebuda',  name='crear_factura_rebuda'),
    url(r'^editar_factura_rebuda/(?P<id_factura>\d+)$', 'editar_factura_rebuda', name='editar_factura_rebuda'),

)
