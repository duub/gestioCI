from django.contrib import admin

from .models import FacturaEmesa, FacturaRebuda, LiniaFactura, QuotaTrimestral

for model in (FacturaEmesa,
              FacturaRebuda,
              LiniaFactura,
              QuotaTrimestral):

    admin.site.register(model)
