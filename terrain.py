"""
This file must be in the same location as manage.py
"""
from unittest import TestCase
from lettuce import after, before, steps, world
from splinter.browser import Browser
from splinter.exceptions import ElementDoesNotExist
from django.test.utils import setup_test_environment, teardown_test_environment
from django.core.management import call_command
from django.db import connection
from django.conf import settings


# --- BEGIN MONKEYPATCH ---
# Search Firefox in OSX
# https://github.com/cobrateam/splinter/issues/377
class Monkey(object):
    def patch(self):
        import sys

        if not sys.platform.startswith('darwin'):
            return

        def find_firefox_path(self):
            from subprocess import check_output
            import os

            firefox_path = check_output(
                ["mdfind", "kMDItemFSName = Firefox.app"]
            ).decode('utf-8').strip()

            return os.path.join(firefox_path, "Contents/MacOS/firefox-bin")

        from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
        FirefoxBinary._get_firefox_start_cmd = find_firefox_path

Monkey().patch()
# --- END MONKEYPATCH ---


# -----
# HOOKS
# -----
@before.harvest
def initial_setup(server):
    # call_command('syncdb', interactive=False, verbosity=0)
    # call_command('flush', interactive=False, verbosity=0)
    # call_command('migrate', interactive=False, verbosity=0)
    # call_command('loaddata', 'all', verbosity=0)
    setup_test_environment()
    world.browser = Browser('firefox')


@after.harvest
def cleanup(server):
    # connection.creation.destroy_test_db(settings.DATABASES['default']['NAME'])
    # teardown_test_environment()
    pass


@before.each_scenario
def reset_data(scenario):
    # Clean up django.
    call_command('flush', interactive=False, verbosity=0)
    # call_command('loaddata', 'all', verbosity=0)

    # TODO: decide a good way to manage fixtures: JSON or python ORM code?
    from django.contrib.auth.models import User
    User.objects.create_user(username='user', password='user')


@after.all
def teardown_browser(total):
    world.browser.quit()


# -------
# HELPERS
# -------
def full_url(url):
    # Fix: https://github.com/gabrielfalcao/lettuce/issues/467
    from lettuce.django import django_url
    return django_url(url)


# ----------
# CORE STEPS
# ----------
@steps
class CoreSteps(object):
    """
    Methods in exclude or starting with _
    will not be considered as step
    """
    exclude = []

    def __init__(self, env, tc):
        self.env = env
        self.tc = tc

    def search(self, queries):
        # We use lambdas to speed up the search process
        # otherwise all queries would be executed first...
        for query in queries:
            el = query()

            if el:
                return el

        raise ElementDoesNotExist

    def dado_que_no_estoy_logeado(self, step):
        '''(?:Y dado|Dado) que no estoy logeado'''
        world.browser.visit(full_url('/accounts/logout/'))

    def accedo_a_la_url(self, step, url):
        '''.* accedo a "([^"]*)"'''
        world.browser.visit(full_url(url))

    def termino_en_la_url(self, step, url):
        '''.* termino en "([^"]*)"'''
        self.tc.assertEqual(world.browser.url, full_url(url))

    def accedo_a_la_pantalla_de_login(self, step):
        '''.* accedo a la pantalla de login'''
        world.browser.visit(full_url('/accounts/login/'))

    def relleno_field_con_value(self, step, field, value):
        '''.* relleno "([^"]*)" con "([^"]*)"'''
        world.browser.find_by_css('[data-test-field="%s"]' % field).fill(value)

    def clico_en_el_botton_text(self, step, text):
        '''.* clico el boton "([^"]*)"'''
        self.search((
            lambda: world.browser.find_by_xpath("//button[contains(text(), '%s')]" % text),
            lambda: world.browser.find_by_css('input[value*="%s"]' % text),
        )).click()

    def termino_en_el_dashboard(self, step):
        '''.* termino en el dashboard'''
        self.tc.assertEqual(world.browser.url, full_url('/'))

    def veo_el_texto_text(self, step, text):
        '''.* veo el texto "([^"]*)"'''
        self.tc.assertTrue(world.browser.is_text_present(text))

CoreSteps(world, TestCase('__init__'))
