GestioCI
========

Acuerdos
--------
Antes de colaborar asegurate de haber leído nuestros consensos de desarrollo en docs/consensos.md .


¿Como colaborar?
----------------

* Clona el repositorio
* Asígnate una tarea
* Trabaja en ella siguiendo el modelo descrito en docs/git.md
* Presenta la branch en la reunión.
* Si es aprobada, puedes hacer un merge a develop.
* Añade tu nick en contributors.txt


Frontend
--------

Leer `gestioci/static/README.md` para ver como trabajar con los CSS y Javascripts.

Backend
-------

* dumpgci.sh exportar dades a fitxers json 
* loadgci.sh importar dades de fitxers json

Desplegar al servidor
---------------------

Els passos a seguir són:

* Baixar codi del repositori
* Instal·lar aplicacions al sistema
* Instal·lar aplicacions bàsiques per montar l'entorn virtual
* Configurar entorn virtual
* Instal·lar dependències dins de l'entorn virtual
* Editar fitxers de configuració
* Configurar nginx, gunicorn i django
